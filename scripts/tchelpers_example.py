"""
This is a sample version the APN text clip helper module, which you can use to
implement script-based macros inserted into text clips. 

Such macros are implemented here as standard scripts, but unlike normal APN
scripts, they simply output text to stdout (typically with the print command).
The text clip handler captures the stdout from these scripts and inserts it 
into the text clip at runtime. 

The syntax for invoking one of these script/macros within the text clip is:
	
	`runScript(tchelpers.scriptname(paramlist))`
	
For example, to invoke the printDateTime() script (given below):
	
	`runScript(tchelpers.printDateTime())`

To make this module 'live', rename it to tchelpers.py, after which it will
be automatically imported from the glue.py module on the next APN launch and
made available to the text clip handler.

To extend this module with additional scripts, either add them directly here,
or put them in separate file(s) in the scripts directory and explicitly 
import them. For example, if you add a script bar() in a module foo.py, you
would include it here via:
	import foo

Then you can either reference the foo.bar functions from this module, or 
directly from a text clip via:
	`runScript(tchelpers.foo.bar())`
	
History:
    Feb 6, 2016 - 1.0.0 - Jack McGregor
        1. Created with printDateTime() example (outputs date as dd.mm.yy, as an
		   alternative to the standard ${PN_DATE}

@author: Jack McGregor (based on help/suggestions by Stuart Tilley & Stephen Funkhouser)
"""
VERSION = "1.0.0"

import datetime

def printDateTime(mask='%d-%b-%y %H:%M'):
    """ Return date in specified strftime format (default: dd-mon-yy hh:mm)
        usage: `runScript(tchelpers.printDateTime({mask}))`
		mask reference: https://docs.python.org/2/library/datetime.html (near bottom of page)
    """
    today = datetime.datetime.today()
    print today.strftime(mask)


