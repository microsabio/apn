###############################################################################
# # Some basic script functions shipped with Programmer's Notepad by various
# # authors including:
# # Simon Steele
# # Scott (wischeese)
# # Benoit

import pn
import scintilla
import string
from pypn.decorators import script
from asb import editor


@script("Sort Lines", "Text")
def SortLines():
    """ Sort Lines (By Stephen Funkhouser """
    # use our editor wrapper to auto Begin/End undo
    e = editor.Editor()
    selected_text = e.select_current_identifier()
    if selected_text:
        selected_lines = selected_text.splitlines(0)
        selected_lines.sort()
        new_text = string.join(selected_lines, e.get_eol())
        # reselect the text (for subsequent toggle)
        e.replace_selection(text=new_text, reselect=True)
    else:
        logger.warning('You must select a variable first')


def SetTarget(e, x, y):
    e.TargetStart = x
    e.TargetEnd = y


@script("Tabs to Spaces", "Text")
def TabsToSpaces():
    editor = scintilla.Scintilla(pn.CurrentDoc())

    tabSpaces = editor.TabWidth
    spaces = ""
    for _ in range(tabSpaces):
        spaces = spaces + " "

    end = editor.Length

    SetTarget(editor, 0, end)
    editor.SearchFlags = 0
    editor.BeginUndoAction()

    pos = editor.SearchInTarget(1, "\t")

    while(pos != -1):
        l1 = editor.TargetEnd - editor.TargetStart
        editor.ReplaceTarget(tabSpaces, spaces)

        # adjust doc length
        end = end + tabSpaces - l1
        start = pos + tabSpaces

        if start >= end:
            pos = -1
        else:
            SetTarget(editor, start, end)
            pos = editor.SearchInTarget(1, "\t")

    editor.EndUndoAction()


@script("Upper case", "Text")
def UpperCase():
    """ Convert text to Upper Case by Benoit """
    editor = scintilla.Scintilla(pn.CurrentDoc())
    editor.BeginUndoAction()
    selText = editor.GetTextRange(editor.SelectionStart, editor.SelectionEnd)
    selText = selText.upper()
    editor.ReplaceSel(selText)
    editor.EndUndoAction()


@script("Lower case", "Text")
def LowerCase():
    """ Convert text to Lower Case by Benoit """
    editor = scintilla.Scintilla(pn.CurrentDoc())
    editor.BeginUndoAction()
    selText = editor.GetTextRange(editor.SelectionStart, editor.SelectionEnd)
    selText = selText.lower()
    editor.ReplaceSel(selText)
    editor.EndUndoAction()


@script("Capitalize case", "Text")
def Capitalize():
    """ Capitalise text by Benoit """
    editor = scintilla.Scintilla(pn.CurrentDoc())
    editor.BeginUndoAction()
    selText = editor.GetTextRange(editor.SelectionStart, editor.SelectionEnd)
    selText = selText.capitalize()
    editor.ReplaceSel(selText)
    editor.EndUndoAction()


@script("Title case", "Text")
def TitleCase():
    """ Title case text by Benoit """
    editor = scintilla.Scintilla(pn.CurrentDoc())
    editor.BeginUndoAction()
    selText = editor.GetTextRange(editor.SelectionStart, editor.SelectionEnd)
    selText = selText.title()
    editor.ReplaceSel(selText)
    editor.EndUndoAction()


@script("Invert case", "Text")
def InvertCase():
    editor = scintilla.Scintilla(pn.CurrentDoc())
    editor.BeginUndoAction()
    selText = editor.GetTextRange(editor.SelectionStart, editor.SelectionEnd)
    selText = selText.swapcase()
    editor.ReplaceSel(selText)
    editor.EndUndoAction()
