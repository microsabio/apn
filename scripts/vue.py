"""
This is a simple collection of scripts emulating certain popular VUE commands, made available
for those who can't help themselves from using those old commands. 

Notes:
    - Although the script titles show the VUE equivalent control codes, e.g. "(^B)", 
      the actual association to the keyboard command is determined by the current keymap.
      APN is now released with two keymap variationsmust be set up via the 
      Tools > Options > Keyboard dialog.  
      
    - Other popular VUE commands that you may want to remap (but which don't require scripts):
        ^H : Editor.CharLeft   (requires removing ^H from Search.Replace)
        ^J : Editor.
       
        LineDown   
        ^K : Editor.LineUp
        ^L : Editor.CharRight  (requires removing from Edit.Line.Delete)
        ^R : Editor.PageUp     (requires removing from Search.Replace)
        ^T : Editor.PageDown   (requires removing from Edit.Line.Transpose)
        ^E : Editor.DocumentEnd
        ^U : Editor.Home       (may require removing from Edit.UpperCase)
        ^N : Editor.LineEnd    (requires removing from File.New)
        
History:
    Sep 3, 2015 - 1.0.0 - Jack McGregor
        1. Created as aid to those who can't stop themselves from using certain VUE kbd commands 

@author: Jack McGregor
"""
VERSION = "1.0.0"

import pn
import scintilla
import string
from pypn.decorators import script

@script("Line Break (^B)", "VUE")
def LineBreak():
    editor = scintilla.Scintilla(pn.CurrentDoc())
    editor.BeginUndoAction()
    editor.NewLine()
    editor.Home()
    editor.Home()
    editor.CharLeft()
    editor.EndUndoAction()

@script("Concatenate (^Shift+O)", "VUE")
# note: to use ^O, you must first delete it as the shortcut to File > Open
def ConcatLine():
    editor = scintilla.Scintilla(pn.CurrentDoc())
    editor.BeginUndoAction()
    curpos = editor.CurrentPos
    editor.LineDown()
    editor.Home()
    editor.DeleteBack()
    editor.WordRightExtend()
    editor.Clear()
    editor.CurrentPos = curpos
    # trick to make sure there is no selection, set pos
    editor.CharRight()
    editor.CharLeft()
    editor.EndUndoAction()

@script("Delete Char (^D)", "VUE")
# note: to use ^D, you must first delete it as the shortcut to Edit.Line.Duplicate
def DelChar():
    editor = scintilla.Scintilla(pn.CurrentDoc())
    editor.BeginUndoAction()
    editor.CharRight()
    editor.DeleteBack()
    editor.EndUndoAction()

@script("Insert Char (^Shift+F)", "VUE")
# note: to use ^F, you must first delete it as the shortcut to Search
def InsChar():
    editor = scintilla.Scintilla(pn.CurrentDoc())
    editor.BeginUndoAction()
    editor.AddText(1, " ")
    editor.CharLeft()
    editor.EndUndoAction()

@script("Start of Line (^U)", "VUE")
# note: like HOME but always goes to the physical start of line
def StartLine():
    editor = scintilla.Scintilla(pn.CurrentDoc())
    editor.BeginUndoAction()
    editor.Home()
    editor.EndUndoAction()

@script("Mark Line (^P)", "VUE")
# note: This is not a full replacement for the VUE ^P, but
# the toggle and extend aspects of that aren't really needed (i.e. obvious in SWEC)
def MarkLine():
    editor = scintilla.Scintilla(pn.CurrentDoc())
    editor.Home()
    editor.LineEndExtend()

@script("Line Up (^K)", "VUE")
# note: This is a workaround/hack for a mysterious problem mapping ^K to an internal command
def LineUp():
    editor = scintilla.Scintilla(pn.CurrentDoc())
    editor.LineUp()

