"""
Description: A-Shell LSM file related functions and classes

Author: Stephen Funkhouser

History:
    Aug 20, 2014 - 1.0.0 - Stephen Funkhouser
        Created
Notes:
"""
VERSION = "1.0.0"
__updated__ = 'Oct 28, 2014'

# always import/setup logger first
import logging
from .logger import AsbLogger
logging.setLoggerClass(AsbLogger)
logger = logging.getLogger(AsbLogger.ASB_LOGGER_NAME)

from collections import OrderedDict
import copy
import csv
import os
import re
import StringIO

from ..asb import decorators, editor, fileasb, structs


def get_lsm_fpath():
    """ Get LSM files path
    """

    docname = editor.Editor().get_current_docname()
    logger.debug('docname: %s' % docname)
    main_fspec = fileasb.get_main_filepath(fspec=docname)
    logger.debug('main_fspec: %s' % main_fspec)

    return '%s.lsm' % (
        os.path.splitext(main_fspec)[0],
    )


class Lsm(object):
    """ Class for accessing data in LSM files """

    def __init__(self, lsm_fpath=''):
        self._editor = editor.Editor()

        self.VERSION_STR = 'V001'
        self.file_path = ''
        self._f = None
        self._version_verified = False
        self._toc = OrderedDict()

        self.file_index_table = OrderedDict()
        self.func_proc_table = OrderedDict()

        logger.debug('lsm_pfath: %s' % lsm_fpath)

        if not lsm_fpath:
            lsm_fpath = get_lsm_fpath()
        if lsm_fpath:
            if os.path.exists(lsm_fpath):
                self.file_path = lsm_fpath
            else:
                logger.error('Invalid LSM file path!')
        else:
            logger.error('LSM file not found!')
        logger.debug('self.file_path: %s' % self.file_path)

        if self.file_path:
            #==================================================================
            # imperative we use 'b' so python doesn't convert the EOL chars to
            # \n.  This keeps the Table of Contents size attribute valid
            #==================================================================
            self._f = open(self.file_path, 'rb')
            #==================================================================
            # open file on a second handle to allow reading from a specific
            # offset in file will iterating file with other handle
            #==================================================================
            self._f2 = open(self.file_path, 'rb')

            self._version_verified = self.__verify_version()
            self.read_toc()
            self.read_table_file_index()
            self.read_table_function_procedure()

    def get_current_identifier(self):
        """ Custom identifier tweaks for LSM file for differences with tags
            file.  i.e. ordmap in tags doesn't have a trailing parenthese,
            but in the LSM it does
        """
        identifier = self._editor.get_current_identifier()
        if self._editor.is_line_commented(identifier):
            identifier = ''
        if identifier:
            if identifier.startswith('$') and not identifier.endswith('('):
                identifier += '('
            else:
                # remove nested array parentheses because the LSM file
                # doesn't contain them
                identifier = re.sub(
                    pattern=r'\(.*?\)',
                    repl='',
                    string=identifier,
                )
        return identifier

#     @decorators.logging_debug_reset
    def get_variable_structure(self, ident):
        """
        Keyword arguments:
            ident -
        returns:
            instance of a structure, or none
        """

        vardefs = self.get_variable_definition(ident=ident)
        if vardefs:
            return structs.Struct(members=vardefs)
        else:
            logger.error('Not able to find identifier: %s' % ident)

    def get_variable_definition(self, ident):
        """
        Keyword arguments:
            ident -
        returns:
            list of VariableDefinitionRecords if a match was found
        """
        logger.debug('ident: %s ' % ident)

        self.__position_start_of_table_rows(
            table_name=TableOfContentsRecord.VAR_DEF_INDEX
        )

        scope, label = self._editor.get_current_label(with_format=False)
        cur_doc = self._editor.get_current_docname().lower()
        logger.debug('.get_current_label: scope: %s label: %s cur_doc: %s' %
            (
                scope,
                label,
                cur_doc,
            )
        )

        #======================================================================
        # we need to make matches while iterating file, so the file pointer
        # is in the correct location to easily scan for other members
        #======================================================================
        pattern = r'\d+,%s,' % (re.escape(ident))
        matched_candidate = None
        mems = []
        #======================================================================
        # can't just use file as iterator because read-ahead buffer breaks
        # .tell() offset reporting
        #======================================================================
        lrf_offset = self._f.tell()
        gbl_candidate = None
        gbl_offset = 0
        gbl_mems = []
        mod_candidate = None
        mod_offset = 0
        mod_mems = []
        for ln in iter(self._f.readline, ''):
            ln = ln.strip()
            # capture all potential members from every new map level 1
            if ln.startswith('1'):
                mems = []
            mems.append(ln)

            if re.match(pattern=pattern, string=ln) or ln.endswith(ident):
                c = self.__convert_str_to_csv(
                    cstr=ln,
                    return_type=object,
                    csv_conv_func=structs.VariableDefinitionRecord.csv_row_to_obj
                )
                file_path = self.file_index_table.get(c.file_index, '')
                if file_path:
                    file_path = file_path.file_path
                logger.debug('candidate: %s' % c)

                logger.debug('c.file_index: %s c.scope: %s' %
                    (
                        c.file_index,
                        c.scope
                    )
                )
                logger.debug('c: file_path: %s' % file_path)

                # check candidate matches for scope match
                if scope == self._editor.SCOPE_GLOBAL:
                    if c.scope == 0:
                        matched_candidate = c
                        break

                elif scope == self._editor.SCOPE_MODULE:
                    #==========================================================
                    # allow 'global' candidate scope to handle formal
                    # DEFSTRUCTS which are technically global, but can
                    # be defined in modules. The file index and line number
                    # should be correct
                    #==========================================================
                    if c.scope <= 0 and cur_doc.lower() == file_path:
                        matched_candidate = c
                        break

                elif scope == self._editor.SCOPE_FUNC_PROC:
                    if c.scope > 0:
                        signature = self.func_proc_table.get(c.scope, '')
                        if signature:
                            signature = signature.signature
                        if cur_doc.lower() == file_path and \
                                signature.startswith(label):
                            matched_candidate = c
                            break
                #==============================================================
                # regardless of current context scope check for global and
                # module candidate matches to fall back on
                #==============================================================
                if not gbl_candidate and c.scope == 0:
                    gbl_candidate = c
                    gbl_offset = lrf_offset
                    gbl_mems = copy.deepcopy(mems)
                elif not mod_candidate and c.scope < 0 and \
                        cur_doc.lower() == file_path:
                    mod_candidate = c
                    mod_offset = lrf_offset
                    mod_mems = copy.deepcopy(mems)
            lrf_offset = self._f.tell()

        if logger.getEffectiveLevel() <= logging.DEBUG:
            logger.debug('matched_candidate: %s' % matched_candidate)
            if not matched_candidate:
                logger.debug('mod_offset: %s mod_candidate: %s' %
                    (
                        mod_offset,
                        mod_candidate,
                    )
                )
                logger.debug('mod_mems: %s ' % (mod_mems))
                logger.debug('gbl_offset: %s gbl_candidate: %s' %
                    (
                        gbl_offset,
                        gbl_candidate,
                    )
                )
                logger.debug('gbl_mems: %s ' % (gbl_mems))

        if not matched_candidate and mod_candidate:
            matched_candidate = mod_candidate
            mems = mod_mems
            self._f.seek(mod_offset)
        elif not matched_candidate and gbl_candidate:
            matched_candidate = gbl_candidate
            mems = gbl_mems
            self._f.seek(gbl_offset)

        if matched_candidate:
            logger.debug('matched_fpath: %s' %
                (
                    self.file_index_table.get
                    (
                        matched_candidate.file_index, ''
                    ).file_path,
                )
            )

            #==================================================================
            # we've accumulated all members since the last map1.  we need
            # to scan forward to the next map1 to get the rest
            #==================================================================
            # file pointer should be ready to read the next line
            for ln in iter(self._f.readline, ''):
                # capture all potential members from every new map level 1
                if ln.startswith('1'):
                    if self.__check_map_one_for_match(mems=mems, ln=ln):
                        continue
                    break
                mems.append(ln.strip())
            return [
                self.__convert_variable_definition_record(cstr=m)
                for m in mems
            ]
        return []

    def __convert_variable_definition_record(self, cstr):
        return self.__convert_str_to_csv(
            cstr=cstr.strip(),
            return_type=object,
            csv_conv_func=structs.VariableDefinitionRecord.csv_row_to_obj
        )

    def __check_map_one_for_match(self, mems, ln):
        """ This is to handle cases where the current mapped row in the file
            is a map level 1, but was generated by the same A-Shell
            instantiation code as the current map one in the members list

        i.e. file/line number match
        """
        if mems:
            chk_one = self.__convert_variable_definition_record(cstr=mems[0])
            chk_cur = self.__convert_variable_definition_record(cstr=ln)

            logger.debug('chk_one: %s' % chk_one)
            logger.debug('chk_cur: %s' % chk_cur)

            if chk_one.map_level == 1 and \
                    chk_one.file_index == chk_cur.file_index and \
                    chk_one.line_number == chk_cur.line_number:
                return True
        return False

    def __check_next_map_one_for_match(self, ln):
        """ This is to handle cases where the next mapped row in the file
            is a map level 1, but was generated by the same A-Shell
            instantiation code.

        i.e. file/line number match
        """
        chk_cur = self.__convert_variable_definition_record(cstr=ln)
        self._f2.seek(self._f.tell())
        ln2 = self._f2.readline().strip()
        logger.debug('ln: %s' % ln)
        logger.debug('ln2: %s' % ln2)
        chk_next = self.__convert_variable_definition_record(cstr=ln2)
        logger.debug('chk_cur: %s' % chk_cur)
        logger.debug('chk_next: %s' % chk_next)
        if chk_next.map_level == 1 and \
                chk_next.file_index == chk_cur.file_index and \
                chk_next.line_number == chk_cur.line_number:
            return True
        return False

    @decorators.logging_warning_reset
    def read_table_file_index(self):
        """ read file index table
        """

        self.file_index_table = self.__read_table_generic(
            table_name=TableOfContentsRecord.FILE_INDEX,
            dict_func=FileIndexRecord.csv_row_to_dict_tuple,
        )
        logger.debug('len(self.file_index_table): %s' %
            (
                len(self.file_index_table),
            )
        )
        # debug only iteration
        if logger.getEffectiveLevel() <= logging.DEBUG:
            for k, v in self.file_index_table.iteritems():
                logger.debug('key: %s path: %s' % (k, v.file_path))

    @decorators.logging_warning_reset
    def read_table_function_procedure(self):
        """ read function/procedure table
        """

        self.func_proc_table = self.__read_table_generic(
            table_name=TableOfContentsRecord.FUNC_PROC_INDEX,
            dict_func=FunctionProcedureRecord.csv_row_to_dict_tuple,
        )
        # debug only iteration
        if logger.getEffectiveLevel() <= logging.DEBUG:
            for k, v in self.func_proc_table.iteritems():
                logger.debug('key: %s path: %s' % (k, v.signature))

    def __read_table_generic(self, table_name, dict_func):
        """ read an entire table

        Keyword arguments:
            table_name - table of contents record name
            dict_func  - function to be used to generate tuple (key, value)
                in creating the dictionary
        returns:
            OrderedDict()
        """

        if table_name in self._toc and \
                self._f:
            toc = self._toc[table_name]
            # subtract table header length from overall size
            sz = toc.size - self.__position_start_of_table_rows(table_name)
            fp_str = self._f.read(sz)
            logger.debug('read_size: %s eos: %s' %
                (
                    sz,
                    fp_str[-100:].rstrip(),
                )
            )
            #==================================================================
            # convert a string with embedded EOL chars to an OrderedDict
            #==================================================================
            table_orddict = self.__convert_str_to_csv(
                cstr=fp_str,
                return_type=OrderedDict,
                csv_conv_func=dict_func,
            )

            logger.debug('len(table_orddict): %s' % len(table_orddict))
            return table_orddict
        else:
            logger.debug('File Index not in TOC')
        return OrderedDict()

    def __convert_str_to_csv(self, cstr, return_type, csv_conv_func):
        """ Method to parse a csv string and convert to an iterable
        """

        if not isinstance(cstr, basestring):
            cstr = os.linesep.join(cstr)

        r = csv.reader(StringIO.StringIO(cstr))

        if hasattr(return_type, '__iter__'):
            csv_ret = return_type([
                csv_conv_func(row)
                for row in [c for c in r]
            ])
        else:
            csv_ret = [
                csv_conv_func(row)
                for row in [c for c in r]
            ][0]
        return csv_ret

    def __position_start_of_table_rows(self, table_name):
        """ positions the file handle pointer to the start of the tables data
            rows
        """

        if table_name in self._toc and \
                self._f:
            toc = self._toc[table_name]
            # from_what=0 is the beginning of the file
            self._f.seek(toc.offset, 0)

            # this should be the table header row
            ln = self._f.readline()
            logger.debug('ln_size: %s table_hdr: %s' %
                (
                    len(ln),
                    ln.strip(),
                )
            )
            return len(ln)
        return 0

    def read_toc(self):
        """ read table of contents

        Note, this depends on the file handle pointing to the start line
        """

        self._toc = OrderedDict()
        while True:
            ln = self._f.readline().strip()

            if ln.startswith('00'):
                break
            else:
                toc = TableOfContentsRecord(ln=ln)
                self._toc[toc.table_name] = toc

        # debug only iteration
        logger.debug('len(toc): %s ' % len(self._toc))
        if logger.getEffectiveLevel() <= logging.DEBUG:
            for k, v in self._toc.iteritems():
                logger.debug('key: %s tid: %s size: %s offset: %s' %
                    (
                        k, v.table_id, v.size, v.offset
                    )
                )

    def __verify_version(self):
        """ """
        ln = self._f.readline().strip()
        logger.debug('ln: %s' % ln)
        if ln == '[ASBSYM%s]' % self.VERSION_STR:
            logger.debug('version valid')
            return True
        else:
            logger.critical('version invalid')
            return False

    def __del__(self):
        """  close lsm file if open """
        if self._f:
            try:
                self._f.close()
            finally:
                pass
        if self._f2:
            try:
                self._f2.close()
            finally:
                pass


class TableOfContentsRecord(object):
    """ """

    FILE_INDEX = 'FILIDX'
    FUNC_PROC_INDEX = 'FNPIDX'
    VAR_DEF_INDEX = 'VARDEF'

    rec_names = ['', FILE_INDEX, FUNC_PROC_INDEX, VAR_DEF_INDEX]

    def __init__(self, ln):

        r = ln.split(',')
        self.table_id = int(r[0])
        self.size = int('0x%s' % r[1], 16)
        self.offset = int('0x%s' % r[2], 16)
        self.table_name = self.rec_names[self.table_id]


class FileIndexRecord(object):
    """ """

    def __init__(self, index, file_path):
        self.index = int(index)
        self.file_path = file_path

    @classmethod
    def csv_row_to_dict_tuple(cls, row):
        """ """
        return (
            int(row[0]),
            FileIndexRecord(index=row[0], file_path=row[1])
        )


class FunctionProcedureRecord(object):
    """ """

    def __init__(self, index, signature, file_index, line_number):
        logger.debug('idx: %s file_idx: %s ln#: %s sig: %s' %
            (
                index, file_index, line_number, signature
            )
        )
        self.index = int(index)
        self.signature = signature
        self.file_index = int(file_index)
        self.line_number = int(line_number)

    @classmethod
    def csv_row_to_dict_tuple(cls, row):
        """ """
        return (
            int(row[0]),
            FunctionProcedureRecord(
                index=row[0],
                signature=row[1],
                file_index=row[2],
                line_number=row[3],
            )
        )
