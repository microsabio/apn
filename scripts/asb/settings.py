"""
Local default python settings

@author: Stephen Funkhouser

    History:
        08/12/2014 - 1.0.3 - Stephen Funkhouser
        07/09/2014 - 1.0.2 - Stephen Funkhouser
            Add TRACE_ settings

        07/08/2014 - 1.0.1 - Stephen Funkhouser
            Move settings to class as instance attributes
            add get_settings() to handle usersettings overriding/mixin

        06/30/2014 - 1.0.0 - Stephen Funkhouser
            Created

Use:
    #Use get_settings to return an instance of settings with any user
    overridden values

    abs_settings = settings.get_settings()

    abs_settings.COMMENT_EOL_POS
"""
VERSION = "1.0.3"


def get_settings():
    """ Import usersettings if they exist and override Settings()
    """
    s = Settings()
    try:
        import usersettings
        us = usersettings.Settings()
        s.__dict__.update(us.__dict__)
    except:
        pass
    return s


class Settings(object):

    TRACE_TYPE_TRC_PRINT = 0
    TRACE_TYPE_TRC_PAUSE = 1
    TRACE_TYPE_DBG_PRINT = 2
    TRACE_TYPE_DBG_PAUSE = 3

    typ_strs = {}
    typ_strs[TRACE_TYPE_TRC_PRINT] = 'Trace.Print'
    typ_strs[TRACE_TYPE_TRC_PAUSE] = 'Trace.Pause'
    typ_strs[TRACE_TYPE_DBG_PRINT] = 'Debug.Print'
    typ_strs[TRACE_TYPE_DBG_PAUSE] = 'Debug.Pause'

    def __init__(self):
        """
        """

        #======================================================================
        # specifies the position to place a comment at the end of a
        # line of "normal" width text
        #======================================================================
        self.COMMENT_EOL_POS = 70
        #======================================================================
        # specifies the position to place a comment at the end of a
        # line of "wide" width text
        #======================================================================
        self.COMMENT_EOL_POS_WIDE = 132

        # sets default trace/debug type
        self.TRACE_DEFAUL_TYPE = Settings.TRACE_TYPE_TRC_PRINT
        # set to 0 to not split traces
        self.TRACE_VAR_ROW_CT = 5
        # False - traces will be in order of occurrence in selection
        # True  - traces will be sorted
        self.TRACE_VAR_SORT = False
        # optionally exclude A-Shell functions i.e. SPACE()
        self.TRACE_EXCLUDE_ASHELL_FUNCS = True
        # optionally exclude functions
        self.TRACE_EXCLUDE_FUNCS = False
        # optionally exclude functions that are called like procedures
        self.TRACE_EXCLUDE_FUNCS_CALLED_AS_PROCS = False
        # optionally exclude procedures
        self.TRACE_EXCLUDE_PROCS = True
