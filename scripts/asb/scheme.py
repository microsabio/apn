"""
Description: A-Shell Basic Scheme related

Author: Stephen Funkhouser

History:
    Aug 15, 2014 - 1.0.4 - Stephen Funkhouser
        a. add configure_glue(), Scheme.on_doc_load(),
           and Scheme.set_on_doc_load()
    Aug 12, 2014 - 1.0.3 - Stephen Funkhouser
        a. Add get_matching_outer_parentheses()
    Aug 07, 2014 - 1.0.2 - Stephen Funkhouser
        1. Add reading of custom keyword classes
        2. Add remove_keywords_func()
    Aug 04, 2014 - 1.0.1 - Stephen Funkhouser
        1. Add Scheme.is_keyword()
    Jul 29, 2014 - 1.0.0 - Stephen Funkhouser
        Created
Notes:
"""
VERSION = "1.0.4"

# always import/setup logger first
import logging
from .logger import AsbLogger
logging.setLoggerClass(AsbLogger)
logger = logging.getLogger(AsbLogger.ASB_LOGGER_NAME)

from collections import OrderedDict
import os
import re
from xml.dom import minidom

from pypn import glue

from ..asb import decorators, fileasb

NAME = 'asb'


def configure_glue():
    """ Setup configuration for typing into 'glue' event system
    """
    # ensure a scheme configuration has been created
    glue.getSchemeConfig(name=NAME)

    #==========================================================================
    # setup our event call backs. notice there are no parentheses on the method
    # beause this copies (or aliases) the classmethod to be called back
    #==========================================================================
    glue.schemes[NAME].on_doc_load = Scheme.on_doc_load


class Scheme(object):
    """ A-Shell Basic Scheme Class
    """

    """ use class attribute to share across object instances, so we don't have
        to read XML multiple time
    """

    # class attribute, never reference via self.
    on_doc_load_callbacks = OrderedDict()

    keyword_classes = {}
    keywords_all = {}

    # custom keyword classes all
    custom_keyword_classes = {}
    custom_keywords_all = {}

    # custom keyword classes funcs
    custom_keyword_classes_funcs = {}
    custom_keywords_funcs_all = {}

    # custom keyword classes non-funcs
    custom_keyword_classes_nonfuncs = {}
    custom_keywords_nonfuncs_all = {}

    def __init__(self):
        """
        """
        self.scheme_file = fileasb.get_schemedef_file()
        self._read_keywords()

    @classmethod
    def set_on_doc_load(cls, func):
        """ Add a function to be called on document load
        """
        # cls.on_doc_load_callbacks.append(func)

    @classmethod
    def on_doc_load(cls, doc):
        """ This is the single point of entry for executing on_doc_load scripts
        """
        logger.debug('Title: %s' % doc.Title)
        logger.debug('callbacks: %s' % Scheme.on_doc_load_callbacks)

        # TODO: finalize how on_doc_load_callbacks will be initialized, and
        # called
        # cls.on_doc_load_callbacks.append(func)

    def is_keyword(self, keyword):
        """ check if keyword in any class
        """
        if keyword.encode('ascii').strip().lower() in \
                self.keywords_all.iterkeys():
            return True
        return False

    def is_custom_keyword(self, keyword):
        """ check if keyword in any class
        """
        if keyword.encode('ascii').strip().lower() in \
                self.custom_keywords_all.iterkeys():
            return True
        return False

    def is_custom_keyword_func(self, keyword):
        """ check if keyword in any class
        """
        if keyword.encode('ascii').strip().lower() in \
                self.custom_keywords_funcs_all.iterkeys():
            return True
        return False

    def is_custom_keyword_nonfunc(self, keyword):
        """ check if keyword in any class
        """
        if keyword.encode('ascii').strip().lower() in \
                self.custom_keywords_nonfuncs_all.iterkeys():
            return True
        return False

    @decorators.logging_warning_reset
    def get_matching_outer_parentheses(self, s, p_spos=0, p_epos=0):
        """ get
        """

        logger.debug('s: %s ' % s)
        logger.debug('p_spos: %s p_epos: %s' % (p_spos, p_epos))

        spos = 0
        epos = 0
        if s.find('(') != -1:
            ct = 0
            if p_epos > 0:
                s = s[p_spos:p_epos]
            else:
                s = s[p_spos:]

            logger.debug('s to test: %s' % s)
            for idx, ch in enumerate(s):
                if ch == '(':
                    if ct == 0:
                        spos = idx
                    ct += 1
                elif ch == ')':
                    ct -= 1
                    if ct == 0:
                        epos = idx
                        break
        else:
            logger.debug('No parentheses')

        # if matching parentheses not found clear positions
        if spos > epos:
            spos = 0
            epos = 0

        logger.debug('return: spos=%s, epos=%s -- matched_chars: %s' %
            (spos, epos, s[spos:epos + 1]))
        return (spos, epos)

    def remove_keywords(self, s):
        """ remove any keywords in keywords_all & custom_keywords_nonfuncs_all
        """
        #======================================================================
        # remove custom non-funcs first because they provide more precise
        # matching as they an contain characters that PN keywords can't
        #======================================================================
        s = self.__remove_keywords_for_dict(
            s=s,
            d=Scheme.custom_keywords_nonfuncs_all,
        )
        s = self.__remove_keywords_for_dict(
            s=s,
            d=Scheme.keywords_all,
        )
        return s

    def __remove_keywords_for_dict(self, s, d):
        """ remove any keywords in d
        """
        if s:
            for kw, _ in d.iteritems():
                #==============================================================
                # this pattern matches
                # group 1: any 'non word' char or 'word boundary' char
                # the keyword
                # group 2: any 'non word' char or 'word boundary' char
                # this avoids removing keywords that a 'sub' words
                #==============================================================
                pat = r'(\W|\b)%s(\W|\b)' % kw
                #==============================================================
                # I think the space here is important to ensure we maintain
                # separate identifiers
                #==============================================================
                s = re.sub(pat, '\g<1>\g<2>', s, flags=re.I)
                if not s:
                    break
        return s

    def remove_keywords_func(self, s):
        """ remove any Scheme.custom_keywords_funcs_all keywords

        we need to manually match open/close parentheses to ensure we retain
        the identifier being passed to the keyword function
        """

        logger.debug('s: %s ' % s)

        def rm_kw_run_func(match):
            logger.debug('match: prefix: %s run_func: %s lfovr: %s ' % (
                    match.group('prefix'),
                    match.group('run_func'),
                    match.group('left_over'),
                )
            )
            #==================================================================
            # remove the group match which is the run_func with a trail
            # parentheses
            #==================================================================
            #==================================================================
            # # start parentheses count at 1 because of the trailing
            # parenthese in run_func
            #==================================================================
            pct = 1
            left_over = match.group('left_over')
            if left_over:
                for idx, c in enumerate(left_over):
                    if c == '(':
                        pct += 1
                    elif c == ')':
                        pct -= 1
                        if pct == 0:
                            logger.debug('idx: %s char: %s' % (idx, c,))
                            logger.debug('front: %s' % (left_over[:idx]))
                            logger.debug('back: %s' % (left_over[idx + 1:]))
                            #==================================================
                            # I think the space here is important to ensure
                            # we maintain separate identifiers
                            #==================================================
                            if match.group('prefix'):
                                return '%s %s %s' % (
                                    match.group('prefix'),
                                    left_over[:idx], left_over[idx + 1:]
                                )
                            else:
                                return '%s %s' % (
                                    left_over[:idx],
                                    left_over[idx + 1:],
                                )

        for kw, _ in Scheme.custom_keywords_funcs_all.iteritems():
            pat = r'(?P<prefix>.*\W)(?P<run_func>%s)(?P<left_over>.*)' % \
                re.escape('%s(' % kw)

            logger.debug('kw: %s pat: %s' % (kw, pat))
            while True:
                match = re.search(pat, s, flags=re.I)
                if match:
                    logger.debug('while_search: 1: ')
                    s = rm_kw_run_func(match)
                    logger.debug('while_search: 2: s: %s ' % s)
                else:
                    break
        return s

    @decorators.logging_warning_reset
    def _read_keywords(self):
        """ Read keywords from supplied schemedef file

        Note, all keywords are forced to lowercase for easier list checking
        later
        """

        if not Scheme.keyword_classes and os.path.exists(self.scheme_file):
            xmldoc = minidom.parse(self.scheme_file)
            # returns a list if tags
            tags = xmldoc.getElementsByTagName('keyword-class')
            if tags:
                for tag in tags:
                    key = tag.getAttribute('name').strip()
                    val = tag.firstChild.nodeValue.strip().split(' ')
                    if val:
                        if not isinstance(val, list):
                            val = list(val)
                        val = [v.encode('ascii').strip().lower() for v in
                            val if v.strip()
                        ]
                        logger.debug('key: %s val: %s' % (key, val))
                        Scheme.keyword_classes[key] = val

                Scheme.keywords_all = self.__read_keywords_convert_all(
                    kw_cls_dict=Scheme.keyword_classes,
                )
                logger.debug('all: %s' % Scheme.keywords_all)

            # read custom keyword classes
            tags = xmldoc.getElementsByTagName('custom-keyword-class')
            logger.debug('custom-keyword-class: tags: %s' % (tags))
            if tags:
                for tag in tags:
                    key = tag.getAttribute('name').strip()
                    funcs = tag.getAttribute('funcs').strip()
                    val = tag.firstChild.nodeValue.strip().split(' ')
                    logger.debug('custom-keyword-class name: %s funcs: %r' % \
                        (key, funcs))

                    if val:
                        if not isinstance(val, list):
                            val = list(val)
                        val = [v.encode('ascii').strip().lower() for v in
                            val if v.strip()]
                        logger.debug('val: %s' % val)

                        Scheme.custom_keyword_classes[key] = val
                        if not funcs:
                            Scheme.custom_keyword_classes_nonfuncs[key] = val
                        else:
                            Scheme.custom_keyword_classes_funcs[key] = val

                Scheme.custom_keywords_all = self.__read_keywords_convert_all(
                    kw_cls_dict=Scheme.custom_keyword_classes
                )
                Scheme.custom_keywords_funcs_all = (
                    self.__read_keywords_convert_all(
                        kw_cls_dict=Scheme.custom_keyword_classes_funcs
                    )
                )
                Scheme.custom_keywords_nonfuncs_all = (
                    self.__read_keywords_convert_all(
                        kw_cls_dict=Scheme.custom_keyword_classes_nonfuncs
                    )
                )

                logger.debug('custom-all: %s' % Scheme.custom_keywords_all)
                logger.debug('custom-funcs-all: %s' % (
                        Scheme.custom_keywords_funcs_all
                    )
                )
                logger.debug('custom-nonfuncs-all: %s' % (
                        Scheme.custom_keywords_nonfuncs_all
                    )
                )
        else:
            logger.debug('keywords already read!')

    def __read_keywords_convert_all(self, kw_cls_dict):
        """ Convert dictionary of lists to a flat dictionary.
        """
        return dict(
            [(val, '') for _, cls_val in kw_cls_dict.iteritems()
                for val in cls_val
            ]
        )
