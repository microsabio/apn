"""
Description:
Author: Stephen
History:
    Oct 8, 2014 - 1.0.0 - Stephen
"""
VERSION = "1.0.0"
__updated__ = 'Apr 22, 2015'

# always import/setup logger first
import logging
from .logger import AsbLogger
logging.setLoggerClass(AsbLogger)
logger = logging.getLogger(AsbLogger.ASB_LOGGER_NAME)

import hashlib
import json
import os
import re

from ..asb import decorators, editor, projects


@decorators.logging_critical_reset
def get_structs_dirpath():
    """ Get the directory for structure files

    %TEMP%\APN\<pid>\<project_group>\structs\
    """
    p = projects.Projects()
    d = os.path.join(
        p.get_temp_project_group_dir(),
        'structs',
    )
    if not os.path.exists(d):
        os.makedirs(d)
    return d


def get_structs_html_dirpath():
    """ Get the directory for structure files

    %TEMP%\APN\<pid>\<project_group>\structs_html\
    """
    p = projects.Projects()
    d = os.path.join(
        p.get_temp_project_group_dir(),
        'structs_html',
    )
    if not os.path.exists(d):
        os.makedirs(d)
    return d


# @decorators.logging_debug_reset
def write_struct_file(struct_obj):
    """ Write structure to \structs\ directory via cPickle"""

    logger.debug('struct_obj: %s' % struct_obj)
    if struct_obj:
        h = hashlib.md5()
        h.update(struct_obj.name)
        h.update(struct_obj.docname)
        path = os.path.join(
            get_structs_dirpath(),
            h.hexdigest()
        )
        logger.debug('path: %s' % path)
        with open(path, 'wb') as f:
            f.write(struct_obj.to_JSON())
        return path
    else:
        logger.debug('No structure object to be pickled')


class Struct(object):
    """ A-Shell structured and unstructured object representation """

#     @decorators.logging_debug_reset
    def __init__(self, members):
        logger.debug('len(members)=%s _type: 0x%s struct_name: %s' %
            (
                len(members),
                members[0]._type_hex,
                members[0].struct_name,
            )
        )
        if members[0].struct_name:
            self.name = members[0].struct_name
        else:
            self.name = members[0].variable_name
        self.members = members
        e = editor.Editor()
        self.docname = e.get_current_docname()
        logger.debug('name: %s docname: %s' % (self.name, self.docname))

    def to_JSON(self):
        return json.dumps(
            self,
            default=lambda o: o.__dict__,
            sort_keys=True,
            indent=4
        )

    def __str__(self):
        return 'Struct: %s' % (self.name)


class VariableDefinitionRecord(object):
    """ """

    VAR_X = 0  # Unformatted variable
    VAR_S = 1  # String variable
    VAR_F = 2  # Floating point variable
    VAR_B = 3  # Unsigned binary variable
    VAR_I = 4  # AlphaBasic+ Int variable
    # Collection (assoc. array, map, deque, etc)
    VAR_COLL = 0x0020
    # Subscripted variable bit (traditional or base of ass. array)
    VAR_ARRAY = 0x0040
    VAR_BRANCH = 0x0040  # Branch of an associative array
    VAR_DIMX = 0x0080  # DIMX variable
    VAR_LOCAL = 0x0100  # local variable
    VAR_STATIC = 0x0200  # static variable
    VAR_INITED = 0x0400  # static variable already initialized
    VAR_AUTO = 0x0400  # var was auto-mapped (used only in compiler)
    # DEFSTRUCT structure (never really used but did affect RUN hash!)
    VAR_STRUCT = 0x0800
    VAR_BYREF = 0x0800  # var is a pointer to another var (byref)
    VAR_EXTERN = 0x1000  # var has been temporarily extern'd
    VAR_USED = 0x2000  # var has been referenced in program
    VAR_DYN = 0x4000  # var is dynamically sized
    VAR_DYNARY = 0x8000  # array can be auto-extended

    def __init__(self, map_level, variable_name, _type, size, offset,
            scope, file_index, line_number, struct_name):
        self.map_level = int(map_level)
        self.variable_name = variable_name.strip()
        self._type_hex = _type
        self._type = int('0x%s' % _type, 16)
        self.size = int(size)
        self.offset = int(offset)
        self.scope = int(scope)
        self.file_index = int(file_index)
        self.line_number = int(line_number)
        self.struct_name = struct_name.strip()
        self.type_string = self.get_ashell_var_type()
        self.offset_string = self.get_offset()
        self.leading_spaces = '&nbsp;&nbsp;&nbsp;&nbsp;' * (self.map_level - 1)
        if self.map_level > 1:
            self.viewer_name = re.sub(
                pattern=r"((?:\w'?\$?)+)\.",
                repl="",
                string=self.variable_name,
            )
        else:
            self.viewer_name = self.variable_name

#     @decorators.logging_debug_reset
    def get_ashell_var_type(self):
        # isolate last bit because these are not bitflags they're just
        # integer flags i.e. (VAR_B = 3 which would match VAR_S) if they
        # bitflags
        var_type_bit = self._type_hex[-1]

        logger.debug('var: %s _type_hex=[%s] _bit=[%s]' %
            (
                self.variable_name,
                self._type_hex,
                var_type_bit,
            )
        )

        if var_type_bit == str(self.VAR_S):
            return 's'
        elif var_type_bit == str(self.VAR_F):
            return 'f'
        elif var_type_bit == str(self.VAR_B):
            return 'b'
        elif var_type_bit == str(self.VAR_I):
            return 'i'
        elif var_type_bit == str(self.VAR_X):
            return 'x'

    def get_offset(self):
        if self.map_level == 1:
            return 0
        else:
            return self.offset

    def __str__(self):
        return 'ML: %s %s _type: %s scope: %s l#: %s' % (
            self.map_level,
            self.variable_name,
            self._type,
            self.scope,
            self.line_number,
        )

    @classmethod
    def csv_row_to_obj(cls, row):
        return VariableDefinitionRecord(
            map_level=row[0],
            variable_name=row[1],
            _type=row[2],
            size=row[3],
            offset=row[4],
            scope=row[5],
            file_index=row[6],
            line_number=row[7],
            struct_name=row[8],
        )

    @classmethod
    def csv_row_to_dict_tuple(cls, row):
        """ the key is the scope
        """
        return (
            row[5],
            cls.csv_row_to_obj(row),
        )
