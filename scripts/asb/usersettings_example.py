"""
User override python settings


Copy to usersettings.py to enable settings overriding, and copy any settings from settings.py to change the value.

"""
VERSION = "1.0.0"

class Settings(object):

    def __init__(self):
        """
        """

        # uncomment and change to override default settings
#        self.COMMENT_EOL_POS = 80

        # sets default trace/debug type
#         self.TRACE_DEFAUL_TYPE = Settings.TRACE_TYPE_TRC_PRINT
        # set to 0 to not split traces
#         self.TRACE_VAR_ROW_CT = 5
        # False - traces will be in order of occurrence in selection
        # True  - traces will be sorted
#         self.TRACE_VAR_SORT = True
        # optionally exclude A-Shell functions i.e. SPACE()
#         self.TRACE_EXCLUDE_ASHELL_FUNCS = True
        # optionally exclude functions
#         self.TRACE_EXCLUDE_FUNCS = False
        # optionally exclude functions that are called like procedures
#         self.TRACE_EXCLUDE_FUNCS_CALLED_AS_PROCS = False
        # optionally exclude procedures
#         self.TRACE_EXCLUDE_PROCS = True
