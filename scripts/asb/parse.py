"""
Description:

Author: Stephen Funkhouser

History:
    Aug 19, 2014 - 1.0.7 - Stephen Funkhouser
        1. Bug Fix - line_split_outer_parentheses() was returning at least one
            result if there where not parentheses; which, was resulting in it
            just grabbing the first char available.
    Aug 12, 2014 - 1.0.6 - Stephen Funkhouser
        1. parse_identifiers()
            a. refactor to better handle parenthesized identifiers.
                1. individually parses out all functions/procedures/arrays and
                   the arguments passed to them
            b. Add parameters with defaults
                sort=False
                exclude_ashell_funcs=False
                exclude_func_calls=False
                exclude_proc_calls=True
                exclude_func_called_as_proc=False

    Aug 08, 2014 - 1.0.5 - Stephen Funkhouser
        1. parse_identifiers() add exclude_array_no_element
    Aug 08, 2014 - 1.0.4 - Stephen Funkhouser
        1. parse_identifiers()
            a. use tags file to filter out function/procedure identifiers
            b. don't sort parsed return list. that needs to be handled by
               caller for now.  Possibly refactor if this is used for
               anything more than tracedebug
    Aug 07, 2014 - 1.0.3 - Stephen Funkhouser
        1. Bug Fix -
            a. bug in regex pattern matching individual identifiers
               where it wasn't capturing a DIMX array's member
    Aug 07, 2014 - 1.0.2 - Stephen Funkhouser
        1. Bug Fix -
            a. bug in regex pattern matching function/procedure signature in
               selected text
            b. parsed list sort was indented too far, so it wasn't occuring
               if only function/procedure/xcall was in selected text
    Aug 07, 2014 - 1.0.1 - Stephen Funkhouser
        1. parse_identifiers() - remove A-Shell function keywords
    Jul 30, 2014 - 1.0.0 - Stephen Funkhouser
        Created
Notes:
"""
VERSION = "1.0.7"

# always import/setup logger first
import logging
from .logger import AsbLogger
logging.setLoggerClass(AsbLogger)
logger = logging.getLogger(AsbLogger.ASB_LOGGER_NAME)

import re

from ..asb import decorators, editor, scheme


# @decorators.logging_debug_reset
def parse_identifiers(s, sort=False,
        exclude_ashell_funcs=False,
        exclude_func_calls=False,
        exclude_proc_calls=True,
        exclude_func_called_as_proc=False):
    """ parse a-shell identifiers from string

    Keyword arguments:
        s
        sort - (optional) allows sorting

    returns:
        list - all matched identifiers
    """
    e = editor.Editor()
    rlist = []

    #==========================================================================
    # only process if line has valid quote matching uncommented literal
    # strings
    #==========================================================================
    if s and e.is_line_matched_quotes(s):
        s_obj = scheme.Scheme()

        logger.debug('1: s: %s' % (s))
        # remove continuation characters
        s = s.replace('&', '')
        # collapse tabs/multi spaces to single space
        s = ' '.join(s.split())
        # remove quoted string literals
        s = re.sub(r'".*?"', '', s, flags=re.I)
        logger.debug('2: s: %s' % (s))

        # save a copy of string before we start acquiring tokens because we'll
        # need to get the token positions to properly order them later
        save_str = s

        s = parse_function_procedure_declaration(s, rlist)
        logger.debug('post func/proc decl: s: %s' % s)

        s = parse_xcall(s, rlist)
        logger.debug('post XCALL decl: s: %s' % s)

        if exclude_ashell_funcs:
            s = s_obj.remove_keywords_func(s=s)

        #======================================================================
        # parse out identifiers with parentheses.
        # i.e. (arrays, DIMX arrays, ordmaps, functiontions/procedures)
        #======================================================================
        s = parse_parentheses_identifiers(s, rlist)
        logger.debug('post parse_parentheses_identifiers: s: %s' % s)

        s = __parse_map_statement_adornments(s)
        logger.debug('post parse_parentheses_identifiers: s: %s' % s)

        # parse out individual identifiers
        s = parse_individual_identifiers(s, rlist)
        logger.debug('post parse_individual_identifiers: s: %s' % s)

        # remove any procedure 'calls'
        if exclude_proc_calls:
            rlist = __parse_filter_procedures(
                save_str=save_str,
                rlist=rlist,
                exclude_func_called_as_proc=exclude_func_called_as_proc
            )

        # this needs to be optional
        if exclude_func_calls:
            rlist = __parse_filter_functions(save_str=save_str, rlist=rlist)

        rlist = __parse_filter_arrays_empty_arguments(
            save_str=save_str,
            rlist=rlist
        )

        # remove duplicate elements
        rlist = list(set(rlist))
        if sort:
            rlist = sorted(rlist, key=lambda s: s.lower())
        else:
            # reorder to match occurrences in original string
            rlist = __parse_reorder_token_list(save_str, rlist)

        logger.debug('len(rlist): %s list: %s' % (len(rlist), rlist))
    else:
        logger.warning('Invalid syntax - mismatched quotes')
    return rlist


def __parse_filter_functions(save_str, rlist):
    """ iterate already parsed identifier list and filter out procedures
    """
    loc_list = []
    for ident in rlist:
        # regex search is faster then checking tag file
        if not ident.strip().lower().startswith('fn'):
            loc_list.append(ident)
    return loc_list


# @decorators.logging_debug_reset
def __parse_filter_procedures(save_str, rlist,
        exclude_func_called_as_proc=False):
    """ iterate already parsed identifier list and filter out procedures
    """

    logger.debug('save_str: %r' % save_str)
    logger.debug('exclude_func_called_as_proc: %r' %
        exclude_func_called_as_proc
    )
    loc_list = []
    for ident in rlist:
        logger.debug('ident: %r' % ident)
        #======================================================================
        # regex search is faster then checking tag file using
        # tag_obj.get_reference_is_proc(tag=ident)
        #======================================================================
        pat = r'CALL\s+%s' % re.escape(ident)
        logger.debug('pat: %r' % pat)
        if exclude_func_called_as_proc and \
                re.search(pattern=pat, string=save_str, flags=re.I):
            logger.debug('Exclude Function/Procedure')
            continue
        elif not ident.strip().lower().startswith('fn') and \
                re.search(pattern=pat, string=save_str, flags=re.I):
            logger.debug('Exclude Procedure')
            continue
        loc_list.append(ident)
    return loc_list


# @decorators.logging_debug_reset
def __parse_filter_arrays_empty_arguments(save_str, rlist):
    """ iterate already parsed identifier list and filter out arrays with
        empty argument lists
    """

    logger.debug('save_str: %r' % save_str)
    loc_list = []
    for ident in rlist:
        logger.debug('ident: %r' % ident)
        if ident.find('(') != -1:
            # skip checking check functions, but we don't short-ciruit
            if not ident.strip().lower().startswith('fn'):
                #==============================================================
                # regex search is faster then checking tag file using
                # tag_obj.get_reference_is_proc(tag=ident)
                #==============================================================
                pat = r'CALL\s+%s' % re.escape(ident)
                logger.debug('pat: %r' % pat)
                #==============================================================
                # the identifier has parentheses, isn't immediately
                # proceeded by 'CALL '
                #==============================================================
                if not re.search(
                        pattern=pat,
                        string=save_str,
                        flags=re.I) and \
                            re.search(pattern=r'\(\)', string=ident):
                    logger.debug('Exclude array with empty arguments')
                    continue
            else:
                logger.debug('Include Function - no further investigation')
        # we want to short-circuit any identifier that shouldn't be included
        loc_list.append(ident)
    return loc_list


def parse_individual_identifiers(s, rlist):
    """parse out individual identifiers filtering out keywords

    Keyword arguments:
        s
        rlist - not rlist is mutable, so we can just add to it

    returns:
        str - s without any function/procedure delcarations
    """
    s_obj = scheme.Scheme()
    loc_list = []
    for ident in re.findall(r"(?:\$|\.)?(?:(?:\w'?\.?)+\$?)", s):
        ident = ident.strip()
        if ident and not ident.isdigit() and not \
                s_obj.is_keyword(keyword=ident):
            loc_list.append(ident)
    for l in loc_list:
        rlist.append(l)
        s = s.replace(s, ' ')
    return s


# @decorators.logging_debug_reset
def parse_parentheses_identifiers(s, rlist):
    """ parse out all identifiers with trailing parentheses.  Note, we aren't
        going to attempt to determine identifier type.

    Keyword arguments:
        s
        rlist - not rlist is mutable, so we can just add to it

    returns:
        str - s without any function/procedure delcarations
    """
    logger.debug('start: s: %s' % s)

    if s:
        scheme_obj = scheme.Scheme()

        @decorators.logging_warning_reset
        def parse_paren(s):
            """ get outermost parentheses, and leading identifier
            """
            logger.debug('start: s: %s' % (s))

            contents = ''
            ident = ''
            array_member = ''
            spos, epos = scheme_obj.get_matching_outer_parentheses(s=s,)
            logger.debug('return: spos=%s, epos=%s' % (spos, epos))
            if epos > spos:
                contents = s[spos:epos + 1]
                logger.debug('contents: %s' % (contents))
                if spos > 0:
                    pat = r"(?P<ident>(?:\$|\.)?(?:(?:\w'?\.?)+\$?))%s(?P<array_member>\.(?:(?:\w'?\.?)+\$?))?" % (
                        re.escape(contents),
                    )
                    logger.debug('pat: %s' % (pat))
                    search = re.search(pattern=pat, string=s, flags=re.I)
                    if search:
                        ident = search.group('ident')
                        if search.group('array_member'):
                            array_member = search.group('array_member')

                        logger.debug('ident: %s' % ident)
                        logger.debug('array_member: %s' % array_member)

                        #======================================================
                        # add contents of matched parentheses including the
                        # parentheses to identifier
                        #======================================================
                        ident += contents + array_member
            if ident or contents:
                # remove outer open/close parentheses in returned contents
                if contents:
                    contents = contents[1:-1]
                return True, epos, ident, contents
            else:
                return False, epos, ident, contents

        def line_split_outer_parentheses(ln):
            ls = []
            parens = False
            while True:
                #==============================================================
                # this is a very rough split just to put 'outer' parentheses
                # groups in different list elements
                #==============================================================
                _, epos = scheme_obj.get_matching_outer_parentheses(s=ln)
                if epos > 0:
                    ls.append(ln[:epos + 1])
                    ln = ln[epos + 1:]
                    parens = True
                else:
                    # append last line even though it's not a parentheses group
                    # if we matched other parentheses
                    if parens == True and ln:
                        ls.append(ln[:epos + 1])
                    break
            return ls

        def parse_one(ln):
            """
            """
            loc_list = []
            interal_idents_list = []
            rc, epos, ident, contents = parse_paren(ln)
            logger.debug('ret: parse_paren: rc: %r epos: %s ident: %s ' % (
                    rc, epos, ident
                )
            )
            logger.debug('-----parse_paren: contents: %s' % (contents))
            if ident:
                loc_list.append(ident)
            if contents:
                if contents.find('(') != -1:
                    t_loc_list, t_interal_idents_list = parse_one(contents)
                    loc_list += t_loc_list
                    interal_idents_list += t_interal_idents_list
                else:
                    interal_idents_list.append(contents)
            for ident in loc_list:
                ln = ln.replace(ident, ' ')

            #==================================================================
            # if there's any remaining code on the line add it back because
            # the arguments passed to the function/procedure. Even if they get
            # duplicated on the line they'll be filtered out later
            #==================================================================
            if ln:
                interal_idents_list.append(ln)
            logger.debug('left_over: %s' % ln)
            return loc_list, interal_idents_list

        ls = line_split_outer_parentheses(s)
        logger.debug('len(ls): %s' % (len(ls)))

        loc_list = []
        interal_idents_list = []
        for l in ls:
            logger.debug('l: %s ' % l)
            t_loc_list, t_interal_idents_list = parse_one(l)
            loc_list += t_loc_list
            interal_idents_list += t_interal_idents_list

        logger.debug('len(loc_list): %s len(interal_idents_list): %s' % (
                len(loc_list),
                len(interal_idents_list),
            )
        )
        for l in loc_list:
            logger.debug('loc: %s ' % l)
            rlist.append(l)
            s = s.replace(l, ' ')

        for l in interal_idents_list:
            logger.debug('ii_l: %s ' % l)
            s += ' ' + l
    logger.debug('return: s: %s' % s)
    return s


def parse_function_procedure_declaration(s, rlist):
    """ Parse all function/procedure declaration

    Keyword arguments:
        s
        rlist - not rlist is mutable, so we can just add to it

    returns:
        str - s without any function/procedure delcarations
    """

    if s:
        pat_func_proc = r"(?P<func_proc>(?:function|procedure)\s+.*?\(.*?\)(?:\s+as\s+(?:s\s(?:(?:\w'?)+\$?)|\w\d+))?)"
        while True:
            logger.debug('func/proc loop: s: %s' % (s))
            search = re.search(pat_func_proc, s, flags=re.I)
            if search:
                logger.debug('func_match: span: %s %s' % (
                        search.span(),
                        search.group('func_proc')
                    )
                )
                # add to rlist by parsing function/procedure
                rlist += __parse_function_procedure_declaration(
                    search.group('func_proc')
                )

                logger.debug('func/proc parsed: var_ct: %s list: %s' % (
                    len(rlist), rlist)
                )
                #==============================================================
                # remove matched function/procedure declaration from text
                # string to be parsed
                #==============================================================
                if search.group('func_proc') == s:
                    s = ''
                else:
                    s = s[:search.start()] + s[search.end():]
            else:
                break
            logger.debug('func/proc POST: var_ct: %s list: %s' % (
                len(rlist), rlist)
            )
            logger.debug('func/proc POST: s: %s' % (s))

    return s


def __parse_function_procedure_declaration(s):
    """ Helper in parsing just one function/procedure declaration
    """

    pat = r"^\s*(function|procedure)\s+.*?\("

    if re.search(pat, s, flags=re.I):
        # remove function/procedure plus label if exists
        s = re.sub(pat, "", s, flags=re.I)
        logger.debug('post func: s: %s' % (s))

        #======================================================================
        # function/procedure so we'll remove other adornments that are only
        # for them to simplifier identifier parsing
        #======================================================================

        # remove variable INPUTONLY|OUTPUTONLY declaration
        s = re.sub(r":(?:inputonly|outputonly)", "", s, flags=re.I)

        logger.debug('pre def: s: %s' % (s))

        #======================================================================
        # remove keyword variable defaults. Note, capture group is required
        # function adornment that we want to keep for now
        #======================================================================
        s = re.sub(r"(\s*=.*?)(\)|,|\s+as)", "\g<2>", s, flags=re.I)

        logger.debug('s pre var define: s: %s' % (s))
        #======================================================================
        # # remove variable type definitions. Note, capture group is
        # required function adornment that we want to keep for now
        #======================================================================
        s = re.sub(r"\s+as\s+.*?(,|\))", "\g<1>", s, flags=re.I)

        # remove trailing signature parentheses and type
        s = re.sub(r"\)(\s+as\s+.*$)?", "", s, flags=re.I)

        logger.debug('func/proc final: s: %s' % (s))
        if s:
            #==================================================================
            # Custom parser for functions/procedures
            #
            # The goal is to ensure the parse string is distilled to a
            # comma separated list, but note that there doesn't have to be
            # a comma for one variable
            #==================================================================
            s_obj = scheme.Scheme()
            rlist = [ident.strip() for ident in
                    re.findall(r"\$?(?:\w'?)+\$?", s)
                if ident and not s_obj.is_keyword(keyword=ident)
            ]

            logger.debug('func/proc parsed: var_ct: %s list: %s' % (
                len(rlist), rlist)
            )
            return rlist
    return None


def parse_xcall(s, rlist):
    """ pars all XCALL declarations

    Keyword arguments:
        s
        rlist - not rlist is mutable, so we can just add to it

    returns:
        str - s without any XCALL delcarations
    """

    if s:
        #======================================================================
        # # note, the only reason there would be a trailing comma is if there
        # # was a following string literal that's previously been removed, so
        # # we need to match it to remove it
        #======================================================================

        pat_ash_ident = r"(?:(?:\w'?\.?)+\$?)"
        pat_xcall = r"(?P<xcall>[vr]?xcall\s+\w+(\s*,\s*(%s)?=?(%s)?)+(?!:))" \
            % (pat_ash_ident, pat_ash_ident)
        while True:
            logger.debug('xcall loop: s: %s' % (s))
            search = re.search(pat_xcall, s, flags=re.I)
            logger.debug('xcall loop: search: %s' % (search))
            if search:
                logger.debug('xcall_match: span: %s %s' % (
                        search.span(),
                        search.group('xcall')
                    )
                )
                # add to rlist by parsing xcall
                rlist += __parse_xcall(search.group('xcall'))

                logger.debug('xcall parsed: var_ct: %s list: %s' % (
                    len(rlist), rlist)
                )
                #==============================================================
                # remove matched function/procedure declaration from text
                # string to be parsed
                #==============================================================
                if search.group('xcall') == s:
                    s = ''
                else:
                    s = s[:search.start()] + s[search.end():]
            else:
                break
            logger.debug('xcall POST: var_ct: %s list: %s' % (
                len(rlist), rlist)
            )
            logger.debug('xcall POST: s: %s' % (s))
    return s


def __parse_xcall(s):
    """ Helper in parsing just one XCALL declaration
    """

    pat = r"^\s*[vr]?xcall\s+\w+\b"

    if re.search(pat, s, flags=re.I):
        # remove {V}{R}XCALL plus first identifier
        s = re.sub(pat, "", s, flags=re.I)
        logger.debug('removed xcall and name_ident: s: %s' % (s))

        #======================================================================
        # Custom parser for xcalls
        #
        # The goal is to ensure the parse string is distilled to a
        # comma separated list, but note that there doesn't have to be a
        # comma for one variable
        #======================================================================
        #======================================================================
        # split by comma
        # find all identifiers that are not named parameters in capture group
        # use list comprehension to build the list
        #======================================================================
        s_obj = scheme.Scheme()
        rlist = [ident.strip() for r in s.split(',')
                    for ident in
                        re.findall(r"^\s*(?:\s*.*?=)?(.*)", r)
                    if ident and not s_obj.is_keyword(keyword=ident)
        ]
        logger.debug('xcall parsed: var_ct: %s list: %s' % (len(rlist), rlist))
        return rlist
    return None


def __parse_map_statement_adornments(s):
    """remove any map statements adornments.

        Examples the regex matches
            MAP1 argcnt        ,f,8,.ARGCNT
            MAP1 exitcode      ,f,6
            MAP1 exitcode      ,f
            MAP1 read_inv1'rec ,inventory'record_struct
            MAP1 exitcode      ,f,0
            MAp2 strtexta4     ,s,STR_LEN
            dimx ary(1),x,inventory'record_struct,AUTO_EXTEND

    note, appending space to the end to ensure separation with
    following characeters in text
    """
    s = re.sub(r"(?P<map_and_var>(?:dimx|map\d+)\s+(?:(?:\w'?)+\$?(?:\(.*?\))?))\s*,(?:(?:\w'?)+\$?|[bfisx])(?:,(?:\d+|(?:\w'?)+\$?))?(?:,)?", '\g<map_and_var> ', s, flags=re.I)
    logger.debug('general: rm map adorn s: %s' % (s))
    return s


def is_dimx_array(ident):
    """ Is identifier a dimx array.

    if the identifier has a closing parentheses followed by a trailing member

    returns:
        bool
    """
    # all we need to match is a period and word char after close parentheses
    if re.search(r"\)\.\w", ident):
        return True
    return False


# @decorators.logging_debug_reset
def __parse_reorder_token_list(save_str, rlist):
    """ Use original position in save str to order rlist by occurrence

    Keyword arguments:
        save_str -
        rlist - because we're replacing the list the internal replacement
                breaks the 'connection' between the passed list and the
                new one, so we can't use the mutability of rlist to update
    returns:
        list - reorder rlist
    """

    def get_pos(ident):
        pos = 0
        if ident and ident != save_str:
            search = re.search(
                pattern=r'(^|\W)%s(\W|$)' % re.escape(ident),
                string=save_str
           )
            if search:
                pos = search.start()
        return pos

    # list of tuples (pos, ident)
    slist = []
    if save_str and rlist:
        if logger.getEffectiveLevel() <= logging.DEBUG:
            #==================================================================
            # verbose version of code for debugging - not as fast as
            # list comprehensions
            #==================================================================
            for ident in rlist:
                pos = get_pos(ident=ident)
                logger.debug('re.search: pos: %s ident: %s' % (pos, ident))
                slist.append((pos, ident))
            slist.sort()

            rlist = []
            for pos, ident in slist:
                rlist.append(ident)
                logger.debug('rlist: pos: %s ident: %s' % (pos, ident))
        else:
            slist = sorted([
                    (get_pos(ident=ident), ident)
                    for ident in rlist
                ]
            )
            rlist = [ident for _, ident in slist]
    return rlist
