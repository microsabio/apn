"""
Description: Provides a wrapper for scintilla and pn classes.

Author: Stephen Funkhouser

History:
    Aug 16, 2014 - 1.0.8 - Stephen Funkhouser
        1. add option to pass existing PN iDocument instance to constructor
    Aug 13, 2014 - 1.0.7 - Stephen Funkhouser
        1. get_line_from_linenumber() improve by calling get_line() and adding
        optional continuation
        2. add replace_line(), get_end_of_line_from_linenumber(),
           get_start_of_line_from_linenumber()
    Aug 13, 2014 - 1.0.6 - Stephen Funkhouser
        1. Bug Fix -
            a. is_identifier_str_procedure() refine regex change in 1.0.5 bug
               fix to selective add the closing paretheses depending upon if
               one already exists in the passed identifier
        2. Add select_current_identifier(), replace_selection()
    Aug 12, 2014 - 1.0.5 - Stephen Funkhouser
        1. Bug fixes -
            a. get_current_identifier() wasn't removing trailing parentheses
               and arguements from function/procedure when there wasn't a
               closing parentheses
            b. is_identifier_str_procedure() was needlessly adding an ending
                open parentheses to the regex
    Aug 07, 2014 - 1.0.4 - Stephen Funkhouser
        1. get_line() bug fix in continuation option when there wasn't a
           continuation character in the backward scan
        2. Add clear_selected(), __insert_text_adjancent_to_selection()
        3. __insert_text_adjancent_to_selection() - fixes issue where
           continuation used in getting line wasn't being reflected in
           insertion
    Aug 04, 2014 - 1.0.3 - Stephen Funkhouser
        1. get_line() add continuation option
        2. Add is_line_matched_quotes(), get_selected_text()
            , get_selected_text_start_line_start_pos()
            , get_selected_text_end_line_start_pos(), goto_line_next()
            , insert_text_after_selection(), insert_text_before_selection()

    Jul 29, 2014 - 1.0.2 - Stephen Funkhouser
        1. is_identifier_str_function - bug fix startswith() is case sensitive
           , so we first cast string to lower case
    Jul 29, 2014 - 1.0.1 - Stephen Funkhouser
        1. get_current_identifier - bug fix where a function/procedure returned
           a trailing parentheses group
        2. Add get_current_label()
    Jul 24, 2014 - 1.0.0 - Stephen Funkhouser
        Created
Notes:
"""
VERSION = "1.0.8"
# always import/setup logger first
import logging
from .logger import AsbLogger
logging.setLoggerClass(AsbLogger)
logger = logging.getLogger(AsbLogger.ASB_LOGGER_NAME)

import os
import pn
import re
import scintilla

from ..asb import decorators, fileasb


class Editor():
    """ Class to wrap for both scintilla and pn to provide a combined interface
        to avoid confusion.
        We also implement convenience methods to simpilfy tasks

    Note, you can access the current scintilla object directly via edit_obj.sci
    """

    # for our purposes global and gosub are equivalent
    SCOPE_GLOBAL = 0
    SCOPE_GOSUB = 0
    SCOPE_MODULE = 1
    SCOPE_FUNC_PROC = 2

    INSERT_TXTADJ_TOSELECT_MODE_AFTER = 0
    INSERT_TXTADJ_TOSELECT_MODE_BEFORE = 1

    def __init__(self, doc=None):
        self.sci = None
        self.__set_scintilla()

        # properties for tracking continuation line handling
        self._line_num_cont_start = 0
        self._line_num_cont_end = 0

    def __set_scintilla(self, doc=None):
        if not doc:
            doc = pn.CurrentDoc()
        self.sci = scintilla.Scintilla(doc)

    def get_current_char(self):
        return self.get_char(pos=self.get_current_pos())

    def get_previous_char(self, pos=0):
        if pos == 0:
            pos = self.get_current_pos()
        pos -= 1
        return self.get_char(pos=pos)

    def get_char(self, pos):
        return self.sci.GetTextRange(pos, pos + 1)

    @decorators.logging_warning_reset
    def get_current_docname(self):
        """ Get the document name

        returns:
            str - docname
        """
        # Warning: for some odd reason this is unicode, so needs to be
        # converted to ascii...
        curfile = pn.CurrentDoc().FileName.encode('ascii', 'ignore')
        logger.debug("Current file: %s" % curfile)
        return curfile

    def get_current_scheme(self):
            return pn.CurrentDoc().CurrentScheme.encode('ascii', 'ignore')

    def select_current_identifier(self):
        """ Get the current identifier under the cursor, and select it
        """

        spos = self.sci.SelectionStart
        epos = self.sci.SelectionEnd
        logger.debug('spos: %s epos: %s' % (spos, epos))
        if spos == epos:
            # save for testing of potentially multiple matches
            org_spos = spos
            line_spos = self.get_current_start_of_line_pos()
            logger.debug('org_spos: %s cur_spos_line: %s' % (
                    org_spos,
                    line_spos
                )
            )

            sel_text = self.get_current_identifier()

            # locate position of identifier within the text of the line
            logger.debug('ln_str: %s' % self.get_current_line())
            cur_line = self.get_current_line()

            # find best substring match in current line
            spostions = [m.start() for m in
                re.finditer(re.escape(sel_text), cur_line)
            ]
            if spostions:
                logger.debug('regex match_spos: %s' % spostions)
                if len(spostions) == 1:
                    spos = spostions[0]
                else:
                    # find the best match
                    for sp in spostions:
                        if (sp + line_spos) <= org_spos:
                            spos = sp
                        logger.debug('sp: %s plus_line_spos: %s' % (
                                sp,
                                (sp + line_spos)
                            )
                        )

                logger.debug('selected text start position in line text: %s'
                     % spos)

                #==============================================================
                # convert to position with file by adding position in line
                # with start of line position in file
                #==============================================================
                spos += line_spos
                epos = spos + len(sel_text)

                # select the identifier
                self.sci.SelectionStart = spos
                self.sci.SelectionEnd = epos
            else:
                sel_text = ''
                logger.error('identifier not found in current line')
        else:
            sel_text = self.sci.GetTextRange(spos, epos)
        return sel_text

    @decorators.logging_warning_reset
    def get_current_identifier(self):
        """ Get identifier under cursor.

        Notes:
            For arrays, return with a trailing "(" (allowing a scalar and array
            of the same name to be distinguished.)

            For scalars, functions, and procedures, return without a trailing
            "(". (There could be a collision between a procedure name and a
            variable but that seems very unlikely.)

        Warning to caller:
            if you're going to use an array reference in a regex pattern
            , you'll need to escape the trailing parentheses, i.e. "array\(".
            (better yet: use re.escape() function)
        returns:
            str - entire variable where cursor is
        """

        #======================================================================
        # TODO: possible improvement to check if selection is a valid
        # identifier first if more than one char selected. this would allow
        # the user to select a define statement in an array definition
        # and goto it's definition
        #======================================================================
        identifier = ""
        cur_pos = self.get_current_pos()
        spos = self.get_current_start_of_line_pos()
        ch = self.get_current_char()
        logger.debug("char at cursor : %s  cur_pos: %s sline: %s" % (
                ch, cur_pos, spos
            )
        )

        """
        if identifier was already selected, the next char is probably beyond it
        ; if so restart the search one char to the left. (Unless we are at
        start of line)
        """
        if not re.match("[a-z0-9\.'_\$]", ch, re.I):
            # if we are at start of line already
            if cur_pos <= spos:
                return ""  # then nothing to return
            cur_pos -= 1
            ch = self.get_char(pos=cur_pos)

        sav_pos = cur_pos

        while re.match("[a-z0-9\.'_\$\(\)]", ch, re.I) and cur_pos >= spos:
            #==================================================================
            # We want to end scanning left if ch is "(" and the proceeding
            # characters are a function or procedure label. We don't want to
            # consume the leading parentheses
            #
            # Examples:
            # CALL fn'history_get_items'by'customer(
            # item = fn'history_get_items'by'customer(
            # if (fn'history_get_items'by'customer$(
            # if fn'history_get_items'by'customer$(
            # if (fn'other'func$(p1,fn'history_get_items'by'customer$(
            # if (fn'other'func$(p1=p1,p2$=fn'history_get_items'by'customer$(
            #
            # CALL history_set_items'by'customer(
            #
            # We need to treat functions and procedure different because
            # procedures are only valid with the CALL syntax, and the fn'
            # character match makes it easier to handle the more complicated
            # uses of a function
            #==================================================================
            """
            """

            identifier = ch + identifier

            # test if next left char is (
            left_char = self.get_previous_char(pos=cur_pos)
            if left_char == "(":
                text_range = self.sci.GetTextRange(spos, cur_pos)
                logger.debug("scan-left left_char: %s text_range=%s" % (
                        left_char, text_range
                    )
                )

                search_obj = re.search(
                    r"fn'((?:\w'?)+)\$?\($",
                    text_range,
                    re.I
                )
                if search_obj:
                    logger.debug("scan-left group(0): %s" % (
                            search_obj.group(0)
                        )
                    )
                    break
                else:
                    search_obj = re.search(
                        r"call\s+((?:\w'?)+)\$?\($",
                        text_range,
                        re.I
                    )
                    if search_obj:
                        logger.debug("GetCurrentIdentifier:"
                            "scan-left group(0): %s" % (search_obj.group(0)))
                        break

            #==================================================================
            # # note this isn't actually changing the current position in the
            # scintilla object
            #==================================================================
            cur_pos = cur_pos - 1
            ch = self.get_char(pos=cur_pos)

        logger.debug("after scan-left identifier: %s" % identifier)

        """
        # now go the other way to get the ending position
        # move right to find the ending char
        """
        cur_pos = sav_pos + 1
        ch = self.get_char(pos=cur_pos)  # get current char
        endpos = self.get_end_of_line_pos(pos=cur_pos)
        while re.match("[a-z0-9\.'_\$\(\)]", ch, re.I) and cur_pos <= endpos:
            identifier = identifier + ch
            cur_pos = cur_pos + 1
            text_range = self.sci.GetTextRange(cur_pos, endpos)
            #==================================================================
            # test the reset of the current line to match array's. this
            # pattern requires at least something in the use re.match because
            # we want to search from the start of the string
            #==================================================================
            if ch == '(' and not \
                    re.match(r"(?:(?:,?\s*)(?:\w'?)+)+\)", text_range, re.I):
                break
            else:
                #==============================================================
                # # note this isn't actually changing the current position in
                # the scintilla object
                #==============================================================
                ch = self.get_char(pos=cur_pos)

        logger.debug("after scan-right %s" % identifier)

        """
        handling identifier clean up:
        1. remove leading parentheses
        2. if func/proc or collection
            a. if selected variable ends in "(" remove it
            b. else
                i. remove any trailing parentheses group
        3. else must be a scalar/array
            a. if ends "(" leave
            b. else
                i. if trailing group substitue with just "("
            c. handle signature trailing parenthese case
        """
        if identifier:
            if identifier.startswith('('):
                identifier = identifier[1:]

            # if func/proc or collection
            if self.is_identifier_str_function_or_procedure(
                        ident=identifier
                    ) or self.is_identifier_str_collection(ident=identifier):
                logger.debug('function/proc or collection')
                if identifier.endswith('('):
                    # remove (
                    identifier = identifier[:-1]
                else:
                    # remove trailing parentheses group
                    identifier = re.sub(r"\(.*?\)?$", "", identifier)

            # must be scalar/array
            elif identifier.endswith(')'):
                logger.debug('scalar/array')
                # sub trailing parentheses group for open parentheses
                identifier = re.sub(r"\(.*?\)$", "(", identifier)

                logger.debug('scalar/array')

                #==============================================================
                # # handle the case where an it's a signature that ends it
                # with a parentheses. so there's still a trailing close
                # parentheses
                #==============================================================
                if identifier.endswith(')'):
                    identifier = identifier[:-1]

        logger.debug("return: %s" % identifier)
        return identifier

    def is_identifier_str_collection(self, ident):
        ident = ident.strip()
        if ident.startswith('$'):
            return True
        return False

    def is_identifier_str_function_or_procedure(self, ident):
        if self.is_identifier_str_function(ident=ident):
            logger.debug('return True')
            return True
        elif self.is_identifier_str_procedure(ident=ident):
            logger.debug('return True')
            return True
        return False

    def is_identifier_str_function(self, ident):
        """
        """
        ident = ident.strip()
        if ident.lower().startswith('fn'):
            logger.debug('Function')
            return True
        logger.debug('Not Function')
        return False

    def is_identifier_str_procedure(self, ident):
        """
        """
        ident = ident.strip()
        linbuf = self.get_current_line(rstrip=False)
        logger.debug('linbuf: %s' % linbuf)
        # identifier preceded by call?

        if ident.find('(') != -1:
            pat = r'(?:call|procedure)\s+%s' % re.escape(ident)
        else:
            pat = r'(?:call|procedure)\s+%s\(' % re.escape(ident)
        if re.search(pattern=pat, string=linbuf, flags=re.I):
            logger.debug('Procedure')
            return True
        logger.debug('Not Procedure')
        return False

    @decorators.logging_warning_reset
    def get_current_label(self, with_format=True):
        """ Scan backwards in current file to get the current label.
            Function/Procedure/GOSUB

        Keyword arguments:
            bool - with_format - (Optional)
                                 True  - functions/procedures with parentheses
                                         , and go sub with colon
                                 False - just label text
        returns:
            tuple - element 0 - scope
                  - element 1 - label if matched else current document name
        """
        label = ''
        scope = None
        # get current line number, then iterate back to 0 checking
        for line_num in range(self.get_current_linenumber(), -1, -1):
            line_str = self.get_line_from_linenumber(line_num=line_num).strip()
            if line_str and not self.is_line_commented(line_str=line_str):
                logger.debug('line_num: %s  line: %s' % (line_num, line_str))

                match = re.match(
                    pattern=r"^(?:function|procedure)\s+(?P<label>(?:\w'?\$?)+)\(",
                    string=line_str,
                    flags=re.I
                )
                if match:
                    if with_format:
                        label = '%s()' % match.group('label')
                    else:
                        label = match.group('label')
                    scope = self.SCOPE_FUNC_PROC
                    break
                else:
                    match = re.match(
                        pattern=r"^(?P<label>(?:\w'?\$?)+):",
                        string=line_str,
                        flags=re.I
                    )
                    if match:
                        if with_format:
                            label = '%s:' % match.group('label')
                        else:
                            label = match.group('label')
                        scope = self.SCOPE_GOSUB
                        break
        if not label:
            label = os.path.basename(self.get_current_docname())
            if fileasb.is_main_file(fspec=self.get_current_docname()):
                scope = self.SCOPE_GLOBAL
            else:
                scope = self.SCOPE_MODULE
        logger.debug('return scope: %r label: %s' % (scope, label))
        return (scope, label)

    def is_line_commented(self, line_str):
        """
        """
        if line_str.strip().startswith('!'):
            return True
        return False

    def get_current_line(self, rstrip=True, continuation=False):
        """ Get the current line from the scintilla object

        Keyword arguments:
            rstrip - optionally right strip the line

        returns:
            str - contents of current line
        """
        return self.get_line(
            pos=self.get_current_pos(),
            rstrip=rstrip,
            continuation=continuation
        )

    def get_current_linenumber(self):
        return self.get_linenumber(pos=self.get_current_pos())

    def get_current_pos(self):
        """ Get the current position for the scintilla object
        """
        return self.sci.CurrentPos

    def get_current_start_of_line_pos(self):
        """ Get the start position of the current line without moving the
        current position in the editor

        TODO: create decorator to save/restore current position and selection
        """
        start_pos = self.sci.PositionFromLine(
            self.sci.LineFromPosition(self.get_current_pos())
        )
        return start_pos

    def set_current_start_of_line_pos(self):
        """ Set the current position to the start position of the current line
        """
        start_pos = self.sci.PositionFromLine(
            self.sci.LineFromPosition(self.get_current_pos())
        )
        self.set_current_pos(start_pos)

    def get_current_end_of_line_pos(self):
        """ Get the end position of the current line without moving the
            current position in the editor

        """
        return self.get_end_of_line_pos(pos=self.get_current_pos())

    def get_end_of_line_from_linenumber(self, line_num):
        """ Get the end position of the line at the specified line number
        """
        return self.get_end_of_line_pos(
            pos=self.get_position_from_linenumber(line_num=line_num)
        )

    def get_end_of_line_pos(self, pos):
        """ Get the end position of the line at the specified position
        """
        return self.sci.GetLineEndPosition(self.sci.LineFromPosition(pos))

    @decorators.logging_warning_reset
    def get_line(self, pos, rstrip=True, continuation=False):
        """ Get the line for the specified position in scintilla object

        Keyword arguments:
            pos to get line from
            rstrip - optionally right strip the line
            continuation - (optional) join lines that have continuation ending
                chars while stripping comments
        returns:
            str - contents of line
        """

        if not continuation:
            ln = self.sci.GetLine(self.sci.LineFromPosition(pos))
        else:
            """ join this line with continution lines if applicable
            """
            self._line_num_cont_start = 0
            self._line_num_cont_end = 0

            cont_found = False

            line_num = self.get_linenumber(pos=pos)
            #==================================================================
            # step backwards to check for start of continuation if it's not
            # the current line
            #==================================================================
            line_num -= 1
            logger.debug('step-back line_num')
            while True:
                tmpln = self.strip_comment(ln=self.sci.GetLine(line_num))
                # check for continuation line
                search = re.search("^\s*(?P<line>.*?)\s*\&$", tmpln.rstrip())
                if search:
                    cont_found = True
                    line_num -= 1
                else:
                    #==========================================================
                    # add one line back because this line doesn't have a
                    # continuation
                    #==========================================================
                    line_num += 1
                    break

            self._line_num_cont_start = line_num

            #==================================================================
            # step forward from new line position so we capture the entire
            # continuation line
            #==================================================================
            ln = ''
            while True:
                tmpln = self.strip_comment(ln=self.sci.GetLine(line_num))
                # check for continuation line
                search = re.search("^\s*(?P<line>.*?)\s*\&$", tmpln.rstrip())
                if search:
                    cont_found = True
                    # just grabs text from start line to continuation char
                    ln += search.group('line').rstrip()
                    line_num += 1
                else:
                    ln += tmpln
                    break

            if cont_found:
                self._line_num_cont_end = line_num
            else:
                self._line_num_cont_start = 0
                self._line_num_cont_end = 0

        if rstrip:
            ln = ln.rstrip()

        logger.debug('rstrip: %r ln: %s' % (rstrip, ln))
        return ln

    @decorators.logging_warning_reset
    def strip_comment(self, ln):
        """ strip comments from end of line

        returns:
            str - input line with trailing comments removed
        """

        # check for a starting comment, and then be sure there's a comment
        # possible before search string
        if ln.strip().startswith('!'):
            ln = ''
        elif ln.find('!') != -1:
            # count double quotes up to the first !.
            # If mod 2 = 0 it's not in a string literal so exit
            quote_ct = 0
            for i, ch in enumerate(ln):
                if ch == '"':
                    quote_ct += 1
                elif ch == '!' and quote_ct % 2 == 0:
                        ln = ln[:i - 1].rstrip()
                        break
        logger.debug('stripped: %s' % ln)
        return ln

    def is_line_matched_quotes(self, ln):
        return self.is_line_matched_char(ln, char='"')

    def is_line_matched_char(self, ln, char):
        """ Test is line has properly matches quotes, taking comments into
            account

        returns:
            bool - True  for even number of quotes 0, 2, 4, ....
                   False for odd
        """
        char_ct = 0
        # check for starting comment, and then check for any instance of char
        if not ln.strip().startswith('!') and ln.find(char) != -1:
            # count double quotes up to the first ! then break
            for ch in ln:
                if ch == char:
                    char_ct += 1
                # exit on first sight of comment
                elif ch == '!':
                        break
        if char_ct % 2 == 0:
            return True
        else:
            return False

    def get_line_from_linenumber(self, line_num, rstrip=True,
            continuation=False):
        """ Get the line for the specified line number in scintilla object

        Keyword arguments:
            line_num - line number to get
            rstrip - optionally right strip the line

        returns:
            str - contents of line
        """

        return self.get_line(
            pos=self.get_position_from_linenumber(line_num),
            rstrip=rstrip,
            continuation=continuation,
         )

    def get_start_of_line_from_linenumber(self, line_num):
        """ Get the start position of the line at the specified line number

        This is a wrapper to make it more likely for developers to find the
        "start of line" method
        """
        return self.get_position_from_linenumber(line_num=line_num)

    def get_position_from_linenumber(self, line_num):
        """ Get the end position of the line at the specified line number
        """
        return self.sci.PositionFromLine(line_num)

    def get_linenumber(self, pos):
        """ This is 0 indexed line number directory from scintilla object
        """
        return int(self.sci.LineFromPosition(pos))

    def get_eol(self):
        """ Get end of line char(s) for the scintilla object

        returns:
            str - end of line char(s)
        """

        if self.sci.EOLMode == 0:
            return '\r\n'
        elif self.sci.EOLMode == 1:
            return '\r'
        elif self.sci.EOLMode == 2:
            return '\n'

#     @decorators.logging_debug_reset
    def get_selected_text(self, rm_cont_chars=False, rm_eol_chars=False,
            rm_comments=False):
        """ if the user has selection we'll return that

        returns:
            str - selected text if any
        """
        spos = self.sci.SelectionStart
        epos = self.sci.SelectionEnd

        if epos > spos:
            stext = self.sci.GetTextRange(spos, epos)

            if stext:
                if rm_cont_chars:
                    # remove continuation characters
                    stext = stext.replace('&', '')

                if rm_comments:
                    #==========================================================
                    # split list by EOL, iterate stripping comments, and
                    # concatenate back together with EOL
                    #==========================================================
                    stext = ('%s' % (
                            self.get_eol(),
                        )
                    ).join(
                        [self.strip_comment(ln) for ln in
                            stext.split(self.get_eol()) if ln
                        ]
                    )
                    logger.debug('rm_comments stext: %s' % stext)

                if rm_eol_chars:
                    # remove EOL characters
                    stext = stext.replace(self.get_eol(), ' ')
            logger.debug('selected: %s' % stext)
            return stext
        # reutrn empty string so it's the same type as selected text
        return ""

    def get_selected_text_start_line_start_pos(self):
        """
        """
        return self.sci.PositionFromLine(
            self.sci.LineFromPosition(self.sci.SelectionStart),
        )

    def get_selected_text_end_line_start_pos(self):
        """
        """
        return self.sci.PositionFromLine(
            self.sci.LineFromPosition(self.sci.SelectionEnd),
        )

    def goto_line(self, linenum=0):
        if linenum == 0:
            linenum = self.get_current_linenumber()
        self.sci.GotoLine(linenum)

    def goto_line_from_position(self, pos):
        self.sci.GotoLine(self.sci.LineFromPosition(pos))

    def goto_line_next(self):
        """
        """
        linenum = self.get_current_linenumber() + 1
        self.sci.GotoLine(linenum)

    def set_current_pos(self, pos, set_selection=True):
        """ Set the current position for the scintilla object
        """
        self.sci.CurrentPos = pos
        self.sci.SelectionStart = pos
        self.sci.SelectionEnd = pos

    def execute_for_selection_linenum(self, func):
        """ Execute a function over the current selection.  Function will
            receive the current line number

        func   - function to be executed for the selection
        editor - is an instance of a scintilla editor
        """

        self.sci.BeginUndoAction()
        spos = self.sci.SelectionStart
        epos = self.sci.SelectionEnd

        if epos > spos:
            # if anything selected...
            sline = self.sci.LineFromPosition(spos)
            eline = self.sci.LineFromPosition(epos)
            #==================================================================
            # normalize behavior to treat ending position anywhere on line
            # same as if it was at the start of the next line (i.e. do
            # entire last sel line)
            #==================================================================
            if self.sci.CurrentPos > self.sci.PositionFromLine(eline):
                lines = range(sline, eline + 1)
            else:
                lines = range(sline, eline)
                eline -= 1
        else:
            # just current line
            sline = self.sci.LineFromPosition(self.sci.CurrentPos)
            eline = sline  # jm
            lines = [sline]

        logger.debug('lines: (%d-%d) %s' % (sline, eline, lines))

        # for the selected lines
        for cur_line_num in lines:
            func(cur_line_num)

        if epos > spos:
            self.sci.SelectionStart = spos
            self.sci.SelectionEnd = self.sci.GetLineEndPosition(eline)
            logger.debug('sel start: %d, end: %d' % (
                    spos, self.sci.SelectionEnd
                )
            )
            logger.debug('eline from pos: %d' % (
                    self.sci.LineFromPosition(self.sci.SelectionEnd)
                )
            )
        self.sci.EndUndoAction()

    def clear_selected(self):
        self.sci.SelectionStart = 0
        self.sci.SelectionEnd = 0

    def insert_text_after_selection(self, text, eol=True):
        """
        """
        self.__insert_text_adjancent_to_selection(
            mode=Editor.INSERT_TXTADJ_TOSELECT_MODE_AFTER,
            text=text,
            eol=eol,
        )

    def insert_text_before_selection(self, text, eol=True):
        """
        """
        self.__insert_text_adjancent_to_selection(
            mode=Editor.INSERT_TXTADJ_TOSELECT_MODE_BEFORE,
            text=text,
            eol=eol,
        )

    def replace_line(self, line_num, text):
        """ Replace an entire line of text

        Keyword arguments:
            line_num - line number to be replaced
            text     - full line text to replace existing text
        """

        logger.debug('line_num: %s ' % line_num)
        logger.debug('text: %s ' % text)

        spos = self.sci.SelectionStart
        epos = self.sci.SelectionEnd

        line_spos = self.get_position_from_linenumber(line_num=line_num)
        line_epos = self.get_end_of_line_from_linenumber(line_num=line_num)

        logger.debug('line_spos: %s ' % line_spos)
        logger.debug('line_epos: %s ' % line_epos)

        self.sci.SelectionStart = line_spos
        self.sci.SelectionEnd = line_epos

        self.sci.ReplaceSel(text)

        self.sci.SelectionStart = spos
        self.sci.SelectionEnd = epos

    def replace_selection(self, text, reselect=True):
        """ Wrapper for scintilla ReplaceSel() with convience undo action
            , and optional text reselection
        """

        if text:
            self.sci.BeginUndoAction()
            # save current selection
            spos = self.sci.SelectionStart
            epos = self.sci.SelectionEnd

            self.sci.ReplaceSel(text)

            if reselect:
                # optionally restore current selection
                self.sci.SelectionStart = spos
                self.sci.SelectionEnd = epos
            self.sci.EndUndoAction()
        else:
            logger.warning('No Selection to replace!')

#     @decorators.logging_debug_reset
    def __insert_text_adjancent_to_selection(self, mode, text, eol=True):
        """ Internal helper to combine logic for adjacent inserts for
            selected
        """
        save_pos = self.get_current_pos()

        # continuation takes precedence
        logger.debug('continuation: start %s end: %s' % (
                self._line_num_cont_start,
                self._line_num_cont_end,
            )
        )
        logger.debug('cur_pos: %s cur_spos %s sel_spos: %s' % (
                save_pos,
                self.get_current_start_of_line_pos(),
                self.get_selected_text_start_line_start_pos(),
            )
        )

        if mode == Editor.INSERT_TXTADJ_TOSELECT_MODE_AFTER:
            if self._line_num_cont_end > 0:
                insert_pos = self.sci.PositionFromLine(
                    self._line_num_cont_end
                )
            else:
                insert_pos = self.get_selected_text_end_line_start_pos()

        elif mode == Editor.INSERT_TXTADJ_TOSELECT_MODE_BEFORE:
            if self._line_num_cont_start > 0:
                insert_pos = self.sci.PositionFromLine(
                    self._line_num_cont_start
                )
            else:
                insert_pos = self.get_selected_text_start_line_start_pos()

        # position cursor at start of the line of current start
        # selection position
        self.set_current_pos(insert_pos)
        if eol:
            text += self.get_eol()

        if mode == Editor.INSERT_TXTADJ_TOSELECT_MODE_AFTER:
            # now advance to the next line when inserting after because the
            # current position is at the start of 'last' line and we want
            # to insert after that
            self.goto_line_next()

        self.sci.BeginUndoAction()
        self.sci.AddText(len(text), text)
        self.sci.EndUndoAction()

        # restore original position
        self.set_current_pos(save_pos)

        self.clear_selected()

        # put focus back on current doc (doesn't seem to work)
        pn.CurrentDoc().Activate

    def open_document(self, doc_spec):
        """
        """
        pn.OpenDocument(doc_spec, "asb")

        #======================================================================
        # we need to reset the scintilla object to the now opened one as it
        # should have focus
        #======================================================================
        self.__set_scintilla()
