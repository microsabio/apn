# always import/setup logger first
import logging
from .logger import AsbLogger
logging.setLoggerClass(AsbLogger)
logger = logging.getLogger(AsbLogger.ASB_LOGGER_NAME)

from functools import wraps


def memoize_funcs(obj):
    """ Decorator that caches a function's return value each time it is
        called.
        If called later with the same arguments, the cached value is
        returned, and not re-evaluated.
    """

    cache = obj.cache = {}

    @wraps(obj)
    def memoizer(*args, **kwargs):
        key = str(args) + str(kwargs)
        if key not in cache:
            cache[key] = obj(*args, **kwargs)
        return cache[key]
    return memoizer


def logging_debug_reset(obj):
    """ decorator that gets current log level, sets it to supplied
        loglevel, and restores it after function return
    """
    @wraps(obj)
    def wrapper(*args, **kwargs):
        c_loglevel = logger.getEffectiveLevel()
        logger.setLevel(logging.DEBUG)
        ret = obj(*args, **kwargs)
        logger.setLevel(c_loglevel)
        return ret
    return wrapper


def logging_info_reset(obj):
    """ decorator that gets current log level, sets it to supplied
        loglevel, and restores it after function return
    """
    @wraps(obj)
    def wrapper(*args, **kwargs):
        c_loglevel = logger.getEffectiveLevel()
        logger.setLevel(logging.INFO)
        ret = obj(*args, **kwargs)
        logger.setLevel(c_loglevel)
        return ret
    return wrapper


def logging_warning_reset(obj):
    """ decorator that gets current log level, sets it to supplied
        loglevel, and restores it after function return
    """
    @wraps(obj)
    def wrapper(*args, **kwargs):
        c_loglevel = logger.getEffectiveLevel()
        logger.setLevel(logging.WARNING)
        ret = obj(*args, **kwargs)
        logger.setLevel(c_loglevel)
        return ret
    return wrapper


def logging_error_reset(obj):
    """ decorator that gets current log level, sets it to supplied
        loglevel, and restores it after function return
    """
    @wraps(obj)
    def wrapper(*args, **kwargs):
        c_loglevel = logger.getEffectiveLevel()
        logger.setLevel(logging.ERROR)
        ret = obj(*args, **kwargs)
        logger.setLevel(c_loglevel)
        return ret
    return wrapper


def logging_critical_reset(obj):
    """ decorator that gets current log level, sets it to supplied
        loglevel, and restores it after function return
    """
    @wraps(obj)
    def wrapper(*args, **kwargs):
        c_loglevel = logger.getEffectiveLevel()
        logger.setLevel(logging.CRITICAL)
        ret = obj(*args, **kwargs)
        logger.setLevel(c_loglevel)
        return ret
    return wrapper
