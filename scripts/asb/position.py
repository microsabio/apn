"""
Description: Module to be used with position tracking scripts

Author: Stephen Funkhouser

History:
    Jul 25, 2014 - 1.0.0 - Stephen Funkhouser
        Created
Notes:
"""
VERSION = "1.0.0"

# always import/setup logger first
import logging
from .logger import AsbLogger
logging.setLoggerClass(AsbLogger)
logger = logging.getLogger(AsbLogger.ASB_LOGGER_NAME)

from ..asb import decorators, editor


class Position(object):
    """ Class to provide Document-Position stack and movement

    Note, this we need to reference pos_list as a Class attribute, not
    an instance

    i.e. Position.pos_list not self.pos_list
    """

    # TODO: persist stack across sessions per project_group/workspace
    pos_list = []
    __ptr = 0

#     @decorators.logging_debug_reset
    def push(self):
        key = self._get_positon()
        # if the current position is already on the stack, move it to the top
        if key in Position.pos_list:
            logger.debug('Moving current position to top of stack')
            Position.pos_list.remove(key)
        Position.pos_list.append(key)
        # after push we are always at the last position
        Position.__ptr = len(Position.pos_list)

        self.output_stack()

    def next(self):
        """ move to next location
        """
        Position.__ptr += 1
        self.goto()

    def previous(self):
        """ move to previous location
        """
        Position.__ptr -= 1
        self.goto()

    def clear(self):
        Position.__ptr = 0
        Position.pos_list = []

#     @decorators.logging_debug_reset
    def goto(self):
        if Position.pos_list:
            logger.debug('initial: __ptr: %s len: %s' % (
                    Position.__ptr,
                    len(Position.pos_list)
                )
            )

            self.output_stack()

            # python list index starts at 0
            if Position.__ptr >= len(Position.pos_list):
                # wrap around to the beginning
                Position.__ptr = 0
            elif Position.__ptr < 0:
                Position.__ptr = len(Position.pos_list) - 1

            ptr = Position.__ptr
            logger.debug('adjusted: ptr: %s ' % (ptr))

            file_pos = Position.pos_list[ptr]
            logger.debug('initial: file_pos: %s ' % (file_pos))
            if file_pos:
                fname, fpos = file_pos
                logger.debug("Returning to %s, position %d" % (fname, fpos))

                edit_obj = editor.Editor()

                logger.debug('current: file: %s pos: %s' % (
                        edit_obj.get_current_docname(),
                        edit_obj.get_current_pos()
                    )
                )

                edit_obj.open_document(fname)
                # this positions cur line indicator
                edit_obj.goto_line_from_position(pos=fpos)
                edit_obj.set_current_pos(pos=fpos)

                # remove any selection (resulting possibly from shift key)
                edit_obj.sci.SelectionStart = edit_obj.sci.CurrentPos
                edit_obj.sci.SelectionEnd = edit_obj.sci.CurrentPos
        else:
            logger.warning('No Positions Set!')

    def _get_positon(self):
        """ Get current file and position to use as key in pos_list
        """
        eobj = editor.Editor()

        cur_file = eobj.get_current_docname()
        #======================================================================
        # TODO: add settings option to allow configuring how the
        # saved position works
        #======================================================================
        start_pos = eobj.get_current_start_of_line_pos()
        logger.debug('cur_file: %s start_pos: %s cur_pos: %s' % (
                cur_file,
                start_pos,
                eobj.get_current_pos()
            )
        )
        return [cur_file, start_pos]

#     @decorators.logging_info_reset
    def output_stack(self):
        """
        """

        if Position.pos_list:
            for ptr, p in enumerate(Position.pos_list):
                logger.info('ptr: %-3s file: %s pos: %d' % (ptr, p[0], p[1]))
        else:
            logger.info('No positions saved!')
