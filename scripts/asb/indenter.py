"""
Description: Indenter functions

Author: Stephen Funkhouser

History:
    Aug 16, 2014 - 1.0.1 - Stephen Funkhouser
        1. Bug Fix - indenter was deleting text after cursor on original line
    Aug 16, 2014 - 1.0.0 - Stephen Funkhouser
        Created
Notes:
"""
VERSION = "1.0.1"

# always import/setup logger first
import logging
from .logger import AsbLogger
logging.setLoggerClass(AsbLogger)
logger = logging.getLogger(AsbLogger.ASB_LOGGER_NAME)

from ..asb import decorators, editor, scheme


def get_indenter():
    """ Configure indenter

    returns:
        obj - function to be used as indenter.
    """

    # currently, there's only one indenter, so just return it
    return indenter


# @decorators.logging_debug_reset
def indenter(c, doc):
    """ A-Shell Basic auto indenter.

    Check the leading 'keyword' for the previous line to determine if we should
    indenter, dedent, or do nothing

    keyword arguments:
        c - last enter character
        doc - pn iDocument instance
    """

    e = editor.Editor(doc=doc)
    logger.debug('c: %s' % c)
    logger.debug('doc: %s' % doc)

    cur_lnum = e.get_current_linenumber()
    logger.debug('cur_line_num: %s' % cur_lnum)
    prev_line = e.get_line_from_linenumber(
        line_num=cur_lnum - 1,
        continuation=True,
    )
    logger.debug('prev_linenumber: %s' % prev_line)
    logger.debug('prev_is_line_commented: %s' %
        e.is_line_commented(line_str=prev_line)
    )

    if prev_line and not e.is_line_commented(line_str=prev_line):
        s = scheme.Scheme()
        lead_kw = prev_line.strip().split(' ')[0].lower()
        logger.debug('lead_kw: %s ' % lead_kw)
        logger.debug('use_tabs: %r tab_width cur:  %s' % (
                e.sci.UseTabs, e.sci.TabWidth
            )
        )
        logger.debug('get_indentation cur: %s prev: %s' % (
                e.sci.GetLineIndentation(cur_lnum),
                e.sci.GetLineIndentation(cur_lnum - 1),
            )
        )
        try:
            logger.debug('is_ind: %r is_deind: %r' % (
                    (lead_kw in s.custom_keyword_classes['asb_indenter']),
                    (lead_kw in s.custom_keyword_classes['asb_deindenter']),
                )
            )
            set_indent = False
            new_indent = 0
            if lead_kw in s.custom_keyword_classes['asb_indenter']:
                new_indent = e.sci.GetLineIndentation(cur_lnum) \
                    + e.sci.TabWidth
                set_indent = True
            #==================================================================
            # Not sure how we want to handle the de-indent operation
            #==================================================================
#             elif lead_kw in s.custom_keyword_classes['asb_deindenter']:
#                 new_indent = e.sci.GetLineIndentation(cur_lnum) \
#                     - e.sci.TabWidth
#                 set_indent = True

            logger.debug('set_indent: %r new_indent: %s' % (
                    set_indent, new_indent
                )
            )

            if set_indent:
                #==============================================================
                # SetLineIndentation doesn't seem to work correctly, so having
                # to work around it by just inserting the correct amount
                # characters to place the cursor is the correct position
                #==============================================================
                # this is here commented to show what should work, but doesn't
                # e.sci.SetLineIndentation(
                #    cur_lnum,
                #    new_indent
                # )

                e.sci.BeginUndoAction()
                e.set_current_start_of_line_pos()
                save_text = e.get_current_line().strip()
                e.sci.DelLineRight()
                if e.sci.UseTabs:
                    num_tabs = new_indent / e.sci.TabWidth
                    text = chr(9) * num_tabs
                else:
                    text = ' ' * new_indent
                e.sci.AddText(len(text), text)
                if save_text:
                    #==========================================================
                    # save current position so we can place the cursor back
                    # here after adding original text back
                    #==========================================================
                    logger.debug('pos after indent: %s ' % (
                            e.get_current_pos()
                        )
                    )
                    save_pos = e.get_current_pos()
                    e.sci.AddText(len(save_text), save_text)
                    e.set_current_pos(pos=save_pos)
                e.sci.EndUndoAction()
        except:
            logger.error('asb_indenter and/or asb_deindenter'
                ' keyword classes don\'t exist')
