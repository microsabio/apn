# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'structure_viewer.ui'
#
# Created: Wed Oct 29 15:12:35 2014
#      by: PyQt4 UI code generator 4.11
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_dlgStructureViewer(object):
    def setupUi(self, dlgStructureViewer):
        dlgStructureViewer.setObjectName(_fromUtf8("dlgStructureViewer"))
        dlgStructureViewer.resize(560, 600)
        self.centralwidget = QtGui.QWidget(dlgStructureViewer)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.horizontalLayout = QtGui.QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.tabStructureViewer = QtGui.QTabWidget(self.centralwidget)
        self.tabStructureViewer.setObjectName(_fromUtf8("tabStructureViewer"))
        self.horizontalLayout.addWidget(self.tabStructureViewer)
        dlgStructureViewer.setCentralWidget(self.centralwidget)

        self.retranslateUi(dlgStructureViewer)
        self.tabStructureViewer.setCurrentIndex(-1)
        QtCore.QMetaObject.connectSlotsByName(dlgStructureViewer)

    def retranslateUi(self, dlgStructureViewer):
        dlgStructureViewer.setWindowTitle(_translate("dlgStructureViewer", "APN Structure Viewer", None))

