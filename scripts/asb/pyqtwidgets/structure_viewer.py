"""
Description: A-Shell Structure Viewer

Author: Stephen Funkhouser

History:
    Oct 29, 2014 - 1.0.0 - Stephen Funkhouser
        Create - for now we're just going to display one structure per
        dialog instance.  we aren't going to worry about if another
        instance exists, and we're going to delete the structure file
"""
from _msi import PID_APPNAME
VERSION = "1.0.0"
__updated__ = 'Oct 29, 2014'

DEBUG = True
TESTRUN = 0
PROFILE = 0

import logging
from logger import PyQtWidgetLogger
logging.setLoggerClass(PyQtWidgetLogger)
logger = logging.getLogger(PyQtWidgetLogger.ASB_LOGGER_NAME)
logger.setLevel(logging.DEBUG)

import json
from optparse import OptionParser
import os
import sys

from jinja2 import Environment, FileSystemLoader
from PyQt4 import QtGui, QtCore, QtWebKit

from structure_viewer_ui import Ui_dlgStructureViewer


def launch(opts):
    """ Launch instance of PyQt application """
    app = QtGui.QApplication([''])
    form = StructureViewer(
        pid=opts.pid,
        project_group=opts.project_group,
        structfile=opts.structfile,
    )
    form.show()
    app.exec_()


class StructureViewer(QtGui.QMainWindow):
    """ Stucture Viewer """

    def __init__(self, pid, project_group, structfile):
        super(StructureViewer, self).__init__()
        logger.debug("pid=%s ppg=%s structfile=%s" %
            (
                pid,
                project_group,
                structfile,
            )
        )
        self.pid = pid
        self.project_group = project_group
        self.structfile = structfile
        # used for now with only one structure at a time
        self.tab_name = ''
        self.HTML_PATH = self.get_structs_html_dirpath()
        self.STRUCT_PATH = self.get_structs_dirpath()

        logger.debug('HTML_PATH: %s' % self.HTML_PATH)
        logger.debug('STRUCT_PATH: %s' % self.STRUCT_PATH)

        self.ui = Ui_dlgStructureViewer()
        self.ui.setupUi(self)
        self._initUI()

    def _initUI(self):
        """ UI initialization

        For now handling just one structure file
        """

        sfilepath = os.path.join(self.STRUCT_PATH, self.structfile)

        self.generate_html_file(sfilepath=sfilepath)

    def generate_html_file(self, sfilepath):
        """ Generate an HTML file to be displayed from a structure file """

        logger.debug('sfilepath: %s', sfilepath)

        with open(sfilepath, 'rb') as f:
            struct_json = json.load(f)
            logger.debug('struct_json[name]: %s', struct_json.get('name'))
            self.tab_name = struct_json.get('name')
            logger.debug(
                'template exits: %r',
                os.path.exists(self.get_template_path('structure_viewer.html'))
            )

            env = Environment(loader=FileSystemLoader(self.get_template_dir()))
            template = env.get_template(
                'structure_viewer.html'
            )
            rendered_output = template.render(jsonobj=struct_json)
            if rendered_output:
                hf = self.get_structs_html_path(os.path.basename(sfilepath))
                logger.debug('html_file: %s', hf)
                with open(hf, 'wb') as f:
                    f.write(rendered_output)

                self.process_file(path=hf)

    def process_file(self, path):
        """ """
        self.add_tab(path)

    def add_tab(self, htmlfile):
        view = QtWebKit.QWebView()
        view.connect(
            view,
            QtCore.SIGNAL('loadFinished(bool)'),
            self.load_finished
        )
        view.page().setLinkDelegationPolicy(
            QtWebKit.QWebPage.DontDelegateLinks
        )
        self.ui.tabStructureViewer.setCurrentIndex(
            self.ui.tabStructureViewer.addTab(view, 'loading...')
        )

        hfile = QtCore.QString(os.path.abspath(htmlfile))
#         url = QtCore.QUrl.fromLocalFile(hfile)
        url = QtCore.QUrl(hfile)
        view.load(url)

    def load_finished(self, ok):
        """ """
        index = self.ui.tabStructureViewer.indexOf(self.sender())
        self.ui.tabStructureViewer.setTabText(
            index,
            self.tab_name
        )

    def get_template_path(self, template_file):
        return os.path.join(
            self.get_template_dir(),
            '%s' % template_file
        )

    def get_template_dir(self):
        return os.path.expandvars(
            os.path.join(
                '%APN%',
                'scripts',
                'asb',
                'pyqt',
                'templates',
            )
        )

    def get_temp_project_group_dir(self):
        """ Get the project group temp directory path

        %TEMP%\APN\<pid>\<project_group>\
        """

        return os.path.expandvars(
            os.path.join(
                '%TEMP%',
                'APN',
                self.pid,
                self.project_group
            )
        )

    def get_structs_dirpath(self):
        """ Get the directory for structure files

        %TEMP%\APN\<pid>\<project_group>\structs\
        """
        d = os.path.join(
            self.get_temp_project_group_dir(),
            'structs',
        )
        if not os.path.exists(d):
            os.makedirs(d)
        return d

    def get_structs_html_path(self, structfile):
        return os.path.join(
            self.get_structs_html_dirpath(),
            '%s.html' % structfile
        )

    def get_structs_html_dirpath(self):
        """ Get the directory for structure files

        %TEMP%\APN\<pid>\<project_group>\structs_html\
        """
        d = os.path.join(
            self.get_temp_project_group_dir(),
            'structs_html',
        )
        if not os.path.exists(d):
            os.makedirs(d)
        return d

    def __print_watched(self):
        for d in self.qfsw.directories():
            logger.info("monitoring dir: %s" % d)
        for f in self.qfsw.files():
            logger.info("monitoring file: %s" % f)


def main(argv=None):
    '''Command line options.'''
    program_name = os.path.basename(sys.argv[0])
    program_version = "v0.1"
    program_build_date = "%s" % __updated__

    program_version_string = '%%prog %s (%s)' % (
        program_version, program_build_date
    )
    program_longdesc = ''''''  # optional - give further explanation
    program_license = """ MIT License.  To be determined"""

    if argv is None:
        argv = sys.argv[1:]
    try:
        # setup option parser
        try:
            # first instantiate for python 2.7
            parser = OptionParser(
                version=program_version_string,
                epilog=program_longdesc,
                description=program_license
            )
        except:
            # instantiate without epilog for python 2.4 compatibility
            parser = OptionParser(
                version=program_version_string,
                description=program_license
            )

        parser.add_option(
            "",
            "--pid",
            dest="pid",
            help="set APN process ID",
            metavar="FILE"
        )
        parser.add_option(
            "",
            "--ppg",
            dest="project_group",
            help="set APN process ID",
            metavar="FILE"
        )
        parser.add_option(
            "",
            "--structfile",
            dest="structfile",
            help="set DEFSTRUCT file name"
        )
        # process options
        (opts, _) = parser.parse_args(argv)

        logger.debug("pid=%s ppg=%s structfile=%s" %
            (
                opts.pid,
                opts.project_group,
                opts.structfile,
            )
        )

        if opts.pid and opts.project_group and opts.structfile:
            launch(opts)

    # do nothing for SystemExit
    except SystemExit, e:
        pass

    except Exception, e:
        if DEBUG:
            raise
        else:
            indent = len(program_name) * " "
            sys.stderr.write(program_name + ": " + repr(e) + "\n")
            sys.stderr.write(indent + "  for help use --help\n")
            sys.stderr.write("Exception: " + str(e) + "\n")
            return 2

if __name__ == "__main__":
    if TESTRUN:
        import doctest
        doctest.testmod()
    if PROFILE:
        import cProfile
        import pstats
        profile_filename = 'structure_viewer_profile.txt'
        cProfile.run('main()', profile_filename)
        statsfile = open("profile_stats.txt", "wb")
        p = pstats.Stats(profile_filename, stream=statsfile)
        stats = p.strip_dirs().sort_stats('cumulative')
        stats.print_stats()
        statsfile.close()
        sys.exit(0)
    sys.exit(main())
