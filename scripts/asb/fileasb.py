"""
Description: Open Include file routines

Author: Stephen Funkhouser

History:
    Oct 09, 2014 - 1.0.1 - Jack McGregor
        a. Add get_include_filepath() to get full include spec from file.lst 
Notes:
"""
VERSION = "1.0.1"

# always import/setup logger first
import logging
from .logger import AsbLogger
logging.setLoggerClass(AsbLogger)
logger = logging.getLogger(AsbLogger.ASB_LOGGER_NAME)

import os
import re

from ..asb import decorators, projects


@decorators.memoize_funcs
def get_apn_dir():
    pndir = os.environ['APN']  # find APN directory
    if not pndir:
        pndir = os.getcwd
        logger.warning('APN environment variable not defined!'
            ' Trying current dir')
    return pndir


@decorators.memoize_funcs
def get_lang_file():
    """ Get the languages file should be <pndir>\ctags\AdditionalLanguages.conf
    """
    langfile = os.path.join(get_apn_dir(), 'ctags', 'additionalLanguages.conf')
    logger.debug('langfile: %s' % langfile)
    return langfile


@decorators.memoize_funcs
def get_schemedef_file():
    """ Get the languages file should be <pndir>\schemes\asb.schemedef
    """
    scheme_file = os.path.join(get_apn_dir(), 'schemes', 'asb.schemedef')
    logger.debug('scheme_file: %s' % scheme_file)
    return scheme_file


# This function should not be memoized
@decorators.logging_warning_reset
def get_main_filepath(fspec):
    """ Get main files path
    """
    logger.debug('fspec: %s' % (fspec))
    fspec = fspec.rstrip()
    if not is_main_file(fspec):

        """
        get include file list and find first instance
        """
        proj = projects.Projects()
        include_flist = proj.get_include_list_files()
        for lstfile in include_flist:
            logger.debug('Checking: lstfile: %s' % (lstfile))
            if os.path.exists(lstfile):
                with open(lstfile, 'r') as f:
                    # eed to ensure case insensitive comparison
                    fspec = fspec.lower()
                    for l in f:
                        logger.debug("l: %s" % l.lower())
                        if l.lower().find(fspec) != -1:
                            f.seek(0, 0)  # then the first file in apnfiles.lst
                            main_fspec = f.readline().rstrip()
                            logger.debug('main_fspec: %s' % (main_fspec))
                            return main_fspec
    else:
        # if it's already a main file path just return it
        return fspec
    return ''

# This function should not be memoized
# @decorators.logging_warning_reset
def get_include_filepath(fspec):            # [101]
    """ Get include file path from the %apnpid%\ppg\includes\file.lst
    """
    logger.debug('fspec: %s' % (fspec))
    fspec = fspec.rstrip()
    if not is_main_file(fspec):

        """
        get include file list and find first instance
        """
        proj = projects.Projects()
        include_flist = proj.get_include_list_files()
        for lstfile in include_flist:
            logger.debug('Checking: lstfile: %s' % (lstfile))
            if os.path.exists(lstfile):
                with open(lstfile, 'r') as f:
                    # eed to ensure case insensitive comparison
                    fspec = fspec.lower()
                    for l in f:
                        logger.debug("l: %s" % l.lower())
                        if l.lower().find(fspec) != -1:
                            return l.lower().rstrip()   # [101]
    else:
        # if it's already a main file path just return it
        return fspec
    return ''

@decorators.memoize_funcs
def is_main_file(fspec):
    """ Determine if specified file is a main file, based on
        <pndir>\ctags\AdditionalLanguages.conf --langmap asbmain: list

    Keyword arguments:
        fspec

    returns:
        bool - True/False
    """

    logger.debug('fspec: %s' % (fspec))
    # get extension from fspec
    fspec = fspec.rstrip()
    ext = os.path.splitext(fspec)[1]
    logger.debug('ext: %s' % (ext))
    if ext:
        langfile = get_lang_file()
        if os.path.exists(langfile):
            with open(langfile, 'r') as f:
                for ln in f:
                    if ln.find("--langmap") != -1:
                        logger.debug("Looking for asbmain: ... %s in %s" % (
                                ext, ln
                            )
                        )
                        # format of line: ....asbmain:.ext1.ext2.ext3{,...}
                        if re.search(
                                r"asbmain:.*%s[^a-z0-9]" % (re.escape(ext)),
                                ln,
                                re.I):
                            logger.debug("Returning: True")
                            return True
    else:
        logger.info("%s has no extension; must be auxiliary file" % fspec)
    logger.debug("Returning: False")
    return False


@decorators.memoize_funcs
def amos_to_native_file(amos_spec):
    """ For now we just return the filename portion.

    TODO: actually parse project miame.ini & ersatz.ini to convert amos path
    to full native path
    """

    # now remove {dev:} and {[p,pn]}, i.e. trim it down to file.ext
    search_obj = re.match("(\w*?:){0,1}(?P<fname>[^:[]*)", amos_spec)
    if search_obj:
        fname = search_obj.group('fname').rstrip().lower()
        logging.debug("fname: %s" % fname)
        return fname
