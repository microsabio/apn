"""
Created on Jul 23, 2014

@author: Stephen
"""

import inspect
import logging
import logging.handlers
import os
import pn


class AsbLogger(logging.Logger):
    """
    Custom APN A-Shell Basic logger
    """

    ASB_LOGGER_NAME = 'APNLOG'

    def __init__(self, name=''):
        """
        """
        super(AsbLogger, self).__init__(name)

        self.__log_pn_output = False

        loglevel = logging.WARNING
        log_filename = 'asb.log'

        fname = os.path.join(
            os.path.expandvars('%APN%'),
            'scripts',
            log_filename,
        )

        # must be done at the logger level before handlers are defined
        self.setLevel(loglevel)

        fhandler = logging.handlers.RotatingFileHandler(
            filename=fname,
            maxBytes=10485760,  # 10 MB
            backupCount=5,
        )
        fhandlerFmt = logging.Formatter(
            fmt='%(asctime)s:%(message)s',
            datefmt='%m-%d-%Y %H:%M:%S',
        )
        # %(module)s:%(funcName)s()
        fhandler.setFormatter(fhandlerFmt)

        # define a handler to write messages to the console
        console = logging.StreamHandler()
        consoleFmt = logging.Formatter('%(name)s:%(funcName)s() %(message)s')
        console.setFormatter(consoleFmt)

        # add the handlers to logger
        self.addHandler(fhandler)
        self.addHandler(console)

    def debug(self, msg, *args, **kwargs):
        if self.getEffectiveLevel() <= logging.DEBUG:
            levelstr = 'DEBUG'
            # note, although this isn't DRY we don't want to move module name
            # getting code because it adds another layer the stack

            try:
                caller = inspect.getmodule(inspect.stack()[1][0])
                caller = os.path.basename(caller.__file__)
            except:
                caller = ''

            msg = '%-8s:%s:%s() %s' % (
                levelstr,
                caller,
                self.__call_func(),
                msg
            )
            self.__output(msg=msg)
            super(AsbLogger, self).debug(msg, *args, **kwargs)

    def info(self, msg, *args, **kwargs):
        if self.getEffectiveLevel() <= logging.INFO:
            levelstr = 'INFO'
            # note, although this isn't DRY we don't want to move module name
            # getting code because it adds another layer the stack

            try:
                caller = inspect.getmodule(inspect.stack()[1][0])
                caller = os.path.basename(caller.__file__)
            except:
                caller = ''

            msg = '%-8s:%s:%s() %s' % (
                levelstr,
                caller,
                self.__call_func(),
                msg
            )
            self.__output(msg=msg)
            super(AsbLogger, self).info(msg, *args, **kwargs)

    def warn(self, msg, *args, **kwargs):
        self.warning(msg, *args, **kwargs)

    def warning(self, msg, *args, **kwargs):
        if self.getEffectiveLevel() <= logging.WARNING:
            levelstr = 'WARNING'
            # note, although this isn't DRY we don't want to move module name
            # getting code because it adds another layer the stack

            try:
                caller = inspect.getmodule(inspect.stack()[1][0])
                caller = os.path.basename(caller.__file__)
            except:
                caller = ''

            msg = '%-8s:%s:%s() %s' % (
                levelstr,
                caller,
                self.__call_func(),
                msg
            )
            self.__output(msg=msg)
            super(AsbLogger, self).warning(msg, *args, **kwargs)

    def error(self, msg, *args, **kwargs):
        if self.getEffectiveLevel() <= logging.ERROR:
            levelstr = 'ERROR'
            # note, although this isn't DRY we don't want to move module name
            # getting code because it adds another layer the stack

            try:
                caller = inspect.getmodule(inspect.stack()[1][0])
                caller = os.path.basename(caller.__file__)
            except:
                caller = ''

            msg = '%-8s:%s:%s() %s' % (
                levelstr,
                caller,
                self.__call_func(),
                msg
            )
            self.__output(msg=msg)
            super(AsbLogger, self).error(msg, *args, **kwargs)

    def critical(self, msg, *args, **kwargs):
        if self.getEffectiveLevel() <= logging.CRITICAL:
            levelstr = 'CRITICAL'
            # note, although this isn't DRY we don't want to move module name
            # getting code because it adds another layer the stack

            try:
                caller = inspect.getmodule(inspect.stack()[1][0])
                caller = os.path.basename(caller.__file__)
            except:
                caller = ''

            msg = '%-8s:%s:%s() %s' % (
                levelstr,
                caller,
                self.__call_func(),
                msg
            )
            self.__output(msg=msg)
            super(AsbLogger, self).critical(msg, *args, **kwargs)

    def set_log_pn_output(self, value):
        self.__log_pn_output = value

    def __call_func(self):
        """ Get original caller for debug/info/warning/error/critical
        """
        try:
            return inspect.stack()[2][3]
        except:
            return ''

    def __output(self, msg=''):
        if self.__log_pn_output:
            pn.AddOutput('%s\n' % (
                   str(msg),
                )
            )
