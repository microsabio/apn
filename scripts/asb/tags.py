"""
Description: Provides a wrapper for scintilla and pn classes.

Author: Stephen Funkhouser

History:
    Aug 27, 2014 - 1.0.4 - Jack McGregor
        a. Unmemoize the get_tags_dir() as workaround for problem of it
            failing to recognize when project group has changed 
    Aug 12, 2014 - 1.0.3 - Stephen Funkhouser
        a. Add get_reference_is_func(), get_reference_is_proc()
    Aug 08, 2014 - 1.0.2 - Stephen Funkhouser
        a. Add get_reference_is_func_proc()
        b. get_reference() add parameters
            1. context - allow caller to decide if current editor context
               should be applied
            2. paren_remove - allow caller to decide how any wrongly included
               closing parentheses is removed
    Jul 29, 2014 - 1.0.1 - Stephen Funkhouser
        1. get_reference() - enhance to further clarify context.  detect if
           current tag is a func or procedure and match only func/proc lines
           in tag file and vice versa.
    Jul 24, 2014 - 1.0.0 - Stephen Funkhouser
        Created
Notes:
"""
VERSION = "1.0.3"
# always import/setup logger first
import logging
from .logger import AsbLogger
logging.setLoggerClass(AsbLogger)
logger = logging.getLogger(AsbLogger.ASB_LOGGER_NAME)

import csv
import os
import re

from ..asb import fileasb, decorators, editor, projects


# note: unmemoize to force it to recalc tags dir whenever 
#  file changes (in case project changed)
# @decorators.memoize_funcs
def get_tags_dir():
    """ Get the directory for .tags files created by tagger

    """
    proj = projects.Projects()
    return os.path.expandvars(
        os.path.join(
            proj.get_temp_project_group_dir(),
            'tags',
        )
    )


@decorators.memoize_funcs
def get_tags_filepath(fspec):
    """ Get the .tags file path for fspec

    i.e. \\some_path\dsk2\160001\in04n.bas ->
            %TEMP%\APN\<project_group>\tags\in04n.tags
    """
    fspec = fspec.rstrip()
    return os.path.join(
        get_tags_dir(),
        '%s.%s' % (os.path.splitext(os.path.basename(fspec))[0], 'tags')
    )


# This function should not be memoized
# @decorators.logging_debug_reset
def get_tag_file(fspec):
    """ Get the tag file related to this file spec

    returns
        str - tag file spec if it exists, '' if it doesn't

    """

    logger.debug('fspec: %s' % (fspec))
    fspec = fspec.rstrip()
    if fileasb.is_main_file(fspec):
        tag_fspec = get_tags_filepath(fspec)
        logger.debug('main file: tag_fspec: %s' % (tag_fspec))
        if os.path.exists(tag_fspec):
            return tag_fspec

    main_fspec = fileasb.get_main_filepath(fspec)
    logger.debug('main_fspec: %s' % main_fspec)
    if main_fspec:
        tag_fspec = get_tags_filepath(main_fspec)
        logger.debug('tag_fspec: %s' % (tag_fspec))
        if os.path.exists(tag_fspec):
            return tag_fspec
    return ''


class Tags(object):
    """ Class used for extracting data from a .tags file
    """

    GET_REF_PAREN_REMOV_ARRAY = 0
    GET_REF_PAREN_REMOV_FUNC_PROC = 1

    def __init__(self, tag_fspec=''):
        self.edit_obj = editor.Editor()

        if tag_fspec:
            self.__tag_fspec = tag_fspec
        else:
            self.__tag_fspec = get_tag_file(
                self.edit_obj.get_current_docname()
            )

        if not os.path.exists(self.__tag_fspec):
            self.__tag_fspec = None

        logger.debug('tag_fspec: %s' % self.__tag_fspec)

    def get_reference_is_func(self, tag):
        """ Test if tag is referenced, and if it's a function/procedure

        Keyword arguments:
            tag -- tag reference to find in tag file
        """
        logger.debug('tag: %s ' % (tag))
        tag_ref = self.get_reference(
            tag=tag,
            context=False,
            paren_remove=self.GET_REF_PAREN_REMOV_FUNC_PROC
        )
        if tag_ref and self._code_is_func(code=tag_ref['code']):
            return True
        return False

    def get_reference_is_proc(self, tag):
        """ Test if tag is referenced, and if it's a function/procedure

        Keyword arguments:
            tag -- tag reference to find in tag file
        """
        logger.debug('tag: %s ' % (tag))
        tag_ref = self.get_reference(
            tag=tag,
            context=False,
            paren_remove=self.GET_REF_PAREN_REMOV_FUNC_PROC
        )
        if tag_ref and self._code_is_proc(code=tag_ref['code']):
            return True
        return False

#     @decorators.logging_debug_reset
    def get_reference_is_func_proc(self, tag):
        """ Test if tag is referenced, and if it's a function/procedure

        Keyword arguments:
            tag -- tag reference to find in tag file
        """
        logger.debug('tag: %s ' % (tag))
        tag_ref = self.get_reference(
            tag=tag,
            context=False,
            paren_remove=self.GET_REF_PAREN_REMOV_FUNC_PROC
        )
        if tag_ref and self._code_is_func_proc(code=tag_ref['code']):
            return True
        return False

#     @decorators.logging_debug_reset
    def get_reference(self, tag, context=True, paren_remove=0):
        """

        Keyword arguments:
            tag -- tag reference to find in tag file
            context -- (optional) specifies if we should use the current
                      scintilla editors context to provide a more refined
                      tag match when there are multiple matches
            paren_remove -- (optional) specifies how we should remove any
                      trailing parentheses

            # format is tag<TAB>fspec<TAB>linepattern<TAB>code<TAB>line:###
        """

        logger.debug('tag: %s context: %r paren_remove: %s' % (
                tag,
                context,
                paren_remove
            )
        )
        if self.__tag_fspec:
            """ the 'tag' in the tags file rows doesn't contain a closing
                parentheses so check for one what's removed depends on
                paren_remove mode passed
            """

            # check for closing parentheses without regex
            if tag.find(')') != -1:
                if self.GET_REF_PAREN_REMOV_FUNC_PROC:
                    # if procedure mode remove entire parentheses group
                    tag = re.sub(r'\(.*\)', ' ', tag)
                elif self.GET_REF_PAREN_REMOV_ARRAY:
                    #==========================================================
                    # if array mode remove everything to right of the
                    # open parentheses
                    #==========================================================
                    tag = re.sub(r'\((.*\))', '\g<1>', tag)
                logger.debug('remove trailing ): %s ' % tag)

            tag = tag.rstrip()
            # get structure name for tab text
            with open(self.__tag_fspec, 'r') as f:
                #==============================================================
                # store matches as a dictionary of lists
                #    key - is the fspec lowercased
                #    value - each element of the list is a line matching
                #        in the same file
                # i.e.
                # r ef_match['x'] = [row_dict 0, row_dict 1, row_dict 2]
                #==============================================================
                if context:
                    tag_func_proc = \
                        self.edit_obj.is_identifier_str_function_or_procedure(
                            ident=tag
                        )
                    logger.debug('current identifier func/proc?: %r' %
                        tag_func_proc
                    )

                ref_match = {}
                match_start = False
                for ln in f:
                    match = re.match(r'(?P<tag>.*?)\t(?P<fspec>.*?)\t(?P<line_pattern>.*?)\t(?P<code>\w+)\tline:(?P<line_num>\d+)$', ln, re.I)
                    if match and match.group('tag') == tag:
                        match_start = True

                        if context:
                            #==================================================
                            # test if this match is the same code type
                            # as the current identifier
                            #==================================================
                            code_func_proc = self._code_is_func_proc(
                                code=match.group('code')
                            )
                            if tag_func_proc and not code_func_proc:
                                continue
                            elif not tag_func_proc and code_func_proc:
                                continue

                        row_dict = match.groupdict()
                        logger.debug('row_dict: %s' % row_dict)
                        fname = row_dict['fspec'].lower().strip()
                        if fname in ref_match:
                            ref_match[fname].append(row_dict)
                        else:
                            ref_match[fname] = [row_dict]

                    elif match_start == True:
                        """ The tagger file is sorted, so we can optimize our
                            file iteration by exiting when we don't have a tag
                            match after we've started matching
                        """
                        break

                logger.debug('len(ref_match{}): %d' % (len(ref_match)))

                """ attempt to provide the possible context for tag.
                    Check to see if the current document is a key
                    in the matched references.
                """
                if context:
                    cur_fname = (
                        self.edit_obj.get_current_docname().lower().strip()
                    )
                if context and cur_fname in ref_match:
                    """ If tag is in current file, lets further clarify
                    location by checking all possible matches and picking the
                    best one.

                    1. 1st check for a match on the same row
                    2. 2nd check for closest line number less than the current
                       documents line number
                    """
                    # note, we have to add 1 to current line number here
                    # to match tags file which is 1 indexed instead of 0
                    cur_line_num = self.edit_obj.get_current_linenumber() + 1
                    logger.debug('Identifier in doc: cur_line_num: %s' %
                        cur_line_num
                    )

                    closest_match = []
                    closets_match_line_num = 0

                    if len(ref_match[cur_fname]) > 1:
                        for match in ref_match[cur_fname]:
                            # 1st check for exact line number match
                            match_line_num = int(match['line_num'])
                            if match_line_num == cur_line_num:
                                closest_match = match
                                break
                            #==================================================
                            # 2nd check for closest line number less
                            # than the current
                            # documents line number
                            #==================================================
                            elif match_line_num < cur_line_num and \
                                    match_line_num > closets_match_line_num:
                                closets_match_line_num = match_line_num
                                closest_match = match

                        # handle case where the reference is after
                        if not closest_match:
                            # for now just get the 1st match
                            closest_match = ref_match[cur_fname][0]
                    else:
                        closest_match = ref_match[cur_fname][0]
                    logger.debug('closest_match: %s' % closest_match)
                elif ref_match:
                    closest_match = ref_match.itervalues().next()[0]
                    logger.debug('default result: %s' % closest_match)
                else:
                    closest_match = None
                return closest_match
        else:
            logger.warning('No Tag File spec!')

    def _code_is_func(self, code):
        """
        """
        if code.lower() == 'f':
            return True
        return False

    def _code_is_proc(self, code):
        """
        """
        if code.lower() == 'p':
            return True
        return False

    def _code_is_func_proc(self, code):
        """
        """
        logger.debug('code: %s' % code)
        if code.lower() == 'p' or code.lower() == 'f':
            return True
        return False

#     @decorators.logging_debug_reset
    def get_filespec(self, fspec):
        """
            fspec can be partial

        Notes:
            Using csv tab delimited parsing here; which, should be okay
            because we only care about the first instance of the file
            spec. The "line pattern" field can have embedded tabs which
            breaks this, but it doesn't affect here because the format
            of the file is "tag\tfspec"
        returns:
            str - if fspec matched then the first one is returned
        """
        fspec = fspec.rstrip()
        logger.debug('fspec: %s' % fspec)
        if self.__tag_fspec:
            # get structure name for tab text
            with open(self.__tag_fspec, 'r') as f:
                csv_reader = csv.DictReader(
                    f.readlines(),
                    delimiter='\t',
                    fieldnames=self.__get_field_names(),
                )
                for row_dict in csv_reader:
                    if row_dict['fspec'].find(fspec) != -1:
                        logger.debug('matched: tag: %s fspec: %s' % (
                                row_dict['tag'],
                                row_dict['fspec']
                            )
                        )
                        return row_dict['fspec']
        else:
            logger.warning('No Tag File spec!')

    def __get_field_names(self):
        return ['tag', 'fspec', 'line_pattern', 'code', 'line_num']

    def __remove_line_str_from_line_num(self, lnum):
        return lnum.replace('line:', '')
