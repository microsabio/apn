"""
Description: Utitily functions not providing directly by
    Python's data structures

Author: Stephen Funkhouser

History:
    Aug 08, 2014 - 1.0.1 - Stephen Funkhouser
        1. Add list_reshape()
    Jul 30, 2014 - 1.0.0 - Stephen Funkhouser
        Created
Notes:
"""
VERSION = "1.0.0"

# always import/setup logger first
import logging
from ..logger import AsbLogger
logging.setLoggerClass(AsbLogger)
logger = logging.getLogger(AsbLogger.ASB_LOGGER_NAME)

from itertools import izip_longest


def list_reshape(n, iterable, fillvalue=None):
    """ reshape a flat list into a nested list of lists.  nested lists are of
    length n

    list_reshape(3, 'ABCDEFG', 'x') --> ['ABC', 'DEF', 'Gxx']
    """
    args = [iter(iterable)] * n
    return list(izip_longest(fillvalue=fillvalue, *args))


def list_remove_dups(seq):
    """ remove duplicates from a list while preserving the original order
    """
    if seq:
        seen = set()
        """  assign add method to local variable to improve performance because
             this will avoid looking up method on object for every iteration
        """
        seen_add = seen.add
        if logger.getEffectiveLevel() > logging.DEBUG:
            return [x for x in seq if not (x in seen or seen_add(x))]
        else:
            """ this is only for verbose debugging as the list comprehension
                is much much faster
            """
            l = []
            for x in seq:
                if not (x in seen or seen_add(x)):
                    l.append(x)
                else:
                    logger.debug('duplicate: %s' % x)
            return l
    else:
        return []
