"""
Description:

Author: Stephen Funkhouser

History:
    Oct 30, 2014 - 1.0.1 - Stephen Funkhouser
        use startupinfo instead of shell=True
    Oct 30, 2014 - 1.0.1 - Stephen Funkhouser
        remove dependency on win32 module
    Oct 29, 2014 - 1.0.0 - Stephen Funkhouser
        Created
"""
# TODO: kill process when APN is closed
VERSION = "1.0.0"
__updated__ = 'Oct 30, 2014'

# always import/setup logger first
import logging
from ..logger import AsbLogger
logging.setLoggerClass(AsbLogger)
logger = logging.getLogger(AsbLogger.ASB_LOGGER_NAME)

import os
import subprocess
from ...asb import decorators, projects

import pn


# @decorators.logging_debug_reset
def run_detached_structure_viewer(structfile):
    logger.debug('starting process detached')

    p = projects.Projects()
    logger.debug('pid: %s' % str(pn.PID()))
    logger.debug('ppg: %s' % p.get_current_project_group())
    logger.debug('structfile: %s' % structfile)

    exe_path = os.path.expandvars(
        os.path.join(
            '%APN%',
            'scripts',
            'asb',
            'pyqt',
            'structure_viewer',
            'structure_viewer.exe',
        )
    )
    logger.debug('exe_path: %s' % exe_path)

    startupinfo = subprocess.STARTUPINFO()
    startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW

    subprocess.Popen(
        args=[
            exe_path,
            "--pid=%s" % str(pn.PID()),
            "--ppg=%s" % p.get_current_project_group(),
            "--structfile=%s" % structfile
        ],
        startupinfo=startupinfo,
        stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE,
        shell=True,
    )
    logger.debug('started process detached')
