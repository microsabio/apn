"""
Description: functions related to upper/lower case manipulations.

Author: Jack McGregor

History:
    Aug 14, 2014 - 1.0.1 - Stephen Funkhouser
        1. move scintilla operations to reuseable Editor() methods
        2. Bug Fix - where the selection was being made on the first instance
           of the identifier only, so if the cursor was position in the second
           (or greater) instance of the identifier it was case toggling the
           wrong instance in the line
    Aug 13, 2014 - 1.0.0 - Jack McGregor
        Created
Notes:
    This is somewhat experimental. Objective is to improve the
    functionality of the built-in Edit > Upper and Lower functions
    by adding auto-selection of the identifer and toggling case
"""
import logging
from ..logger import AsbLogger
logging.setLoggerClass(AsbLogger)
logger = logging.getLogger(AsbLogger.ASB_LOGGER_NAME)

# relative import
from ...asb import editor


def toggle():
    """ toggle case of identifier """

    edit_obj = editor.Editor()
    sel_text = edit_obj.select_current_identifier()
    logger.debug('sel_text: %s' % sel_text)

    if sel_text:
        new_text = sel_text.upper()
        if new_text == sel_text:  # if already UPPER, do lower
            new_text = sel_text.lower()
        else:
            new_text = sel_text.lower()
            if new_text == sel_text:  # if already lower do
                new_text = sel_text.title()
            else:
                new_text = sel_text.upper()
        logger.debug('new_text: %s ' % new_text)
        # reselect the text (for subsequent toggle)
        edit_obj.replace_selection(text=new_text, reselect=True)
    else:
        logger.warning('You must select a variable first')
