"""
Description:

Author: Stephen Funkhouser

History:
    Jul 25, 2014 - 1.0.0 - Stephen Funkhouser
        Created
Notes:
"""
VERSION = "1.0.0"

# always import/setup logger first
import logging
from ..logger import AsbLogger
logging.setLoggerClass(AsbLogger)
logger = logging.getLogger(AsbLogger.ASB_LOGGER_NAME)

# relative import
from ...asb import decorators, editor, position


# @decorators.logging_debug_reset
def position_push():
    """ save current file, position """
    pobj = position.Position()
    pobj.push()


def go_back():
    """ return to previous location """
    pobj = position.Position()
    pobj.previous()


def go_forward():
    """ return to next location """
    pobj = position.Position()
    pobj.next()


def go_top():
    """ go to top of doc (wrapper for ^HOME to save position for go_back) """
    # push current position, and then goto top

    pobj = position.Position()
    pobj.push()
    eobj = editor.Editor()
    eobj.sci.DocumentStart()


def go_bottom():
    """ go to top of doc (wrapper for ^END to save position for go_back) """
    # push current position, and then goto bottom

    pobj = position.Position()
    pobj.push()
    eobj = editor.Editor()
    eobj.sci.DocumentEnd()


def clear():
    pobj = position.Position()
    pobj.clear()
