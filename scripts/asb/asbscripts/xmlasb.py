"""
Description:

Author: Stephen Funkhouser

History:
    Jul 29, 2014 - 1.0.1 - Stephen Funkhouser
        Fix bug where edit_obj was directly referencing scintilla methods
        instead of edit_obj.sci.
    Jul 29, 2014 - 1.0.0 - Stephen Funkhouser
        Created
Notes:
"""
VERSION = "1.0.1"

# always import/setup logger first
import logging
from ..logger import AsbLogger
logging.setLoggerClass(AsbLogger)
logger = logging.getLogger(AsbLogger.ASB_LOGGER_NAME)

import re
import string
from xml.parsers import expat

from ...asb import editor


def beautify():
    edit_obj = editor.Editor()
    data = edit_obj.sci.GetText(edit_obj.sci.Length)

    fields = re.split('(<.*?>)', data)
    content = ''
    level = 0
    for f in fields:
        if string.strip(f) == '':
            continue
        if f[0] == '<' and f[1] != '/' and f[-2] != '/':
            content += ' ' * (level * 4) + f + '\n'
            level = level + 1
        elif f[0] == '<' and f[1] != '/' and f[-2] == '/':
            content += ' ' * (level * 4) + f + '\n'
        elif f[:2] == '</':
            level = level - 1
            content += ' ' * (level * 4) + f + '\n'
        else:
            content += ' ' * (level * 4) + f + '\n'

    edit_obj.sci.BeginUndoAction()
    edit_obj.sci.ClearAll()
    edit_obj.sci.AddText(len(content), content)
    edit_obj.sci.EndUndoAction()


def validate():
    edit_obj = editor.Editor()
    text = edit_obj.sci.GetText(edit_obj.sci.Length)

    parser = expat.ParserCreate()

    try:
        parser.Parse(text, True)
    except expat.ExpatError, ex:
        logger.error("Error: " + str(ex))
