"""
Description: Provides a wrapper for scintilla and pn classes.

Author: Stephen Funkhouser

History:
    Jul 29, 2014 - 1.0.1 - Stephen Funkhouser
        1. goto_identifier_definition() - bug fix where trailing
           parentheses was being remove that was already being handled by
           Editor.get_current_identifier()
    Jul 28, 2014 - 1.0.0 - Stephen Funkhouser
        Created
Notes:
"""
VERSION = "1.0.1"
# always import/setup logger first
import logging
from ..logger import AsbLogger
logging.setLoggerClass(AsbLogger)
logger = logging.getLogger(AsbLogger.ASB_LOGGER_NAME)

import re

# relative import
from ...asb import decorators, editor, position, tags


# @decorators.logging_debug_reset
def goto_identifier_definition():
    """ open ++include file (on line where cursor is) """

    edit_obj = editor.Editor()
    identifier = edit_obj.get_current_identifier()
    logger.debug('identifier: %s' % identifier)

    if identifier:
        p = position.Position()
        p.push()

        # if selected variable contains (#). reduce to (
        identifier = re.sub(r"\(.*?\)\..*$", "(", identifier)

        # if var contains . strip it and everything after to just match an
        # instance of a structure to the actual variable
        pos = identifier.find('.')
        if pos != -1:
            identifier = identifier[:pos]

        logger.debug('identifier final: %s' % identifier)
        t = tags.Tags()

        # prepare to search twice in case of array var
        for _ in range(0, 2):
            ref = t.get_reference(tag=identifier)
            if ref:
                edit_obj.open_document(ref['fspec'])
                edit_obj.goto_line(linenum=int(ref['line_num']) - 1)
            elif identifier.endswith('('):
                logger.info(
                    "%s not found, trying again with non-array version" %
                    identifier
                )
                identifier = identifier[:-1]
            else:
                logger.warning("%s - Variable definition not found. You may "
                    "need to re-save main file." % identifier)
                break
    else:
        logger.warning('You must select a variable first')
