"""
Description:

Author: Stephen Funkhouser

History:
    Aug 15, 2014 - 1.0.3 - Stephen Funkhouser
        1. add toggle_type()
    Aug 13, 2014 - 1.0.2 - Stephen Funkhouser
        1. trace_debug() - get trace type from settings
           (trace.print, trace.pause, debug.print, debug.pause)
    Aug 08, 2014 - 1.0.1 - Stephen Funkhouser
        1. honor trace settings options
    Jul 30, 2014 - 1.0.0 - Stephen Funkhouser
        Created
Notes:
"""
VERSION = "1.0.3"

# always import/setup logger first
import logging
from ..logger import AsbLogger
logging.setLoggerClass(AsbLogger)
logger = logging.getLogger(AsbLogger.ASB_LOGGER_NAME)

from ...asb import decorators, editor, parse, settings, trace
from ..utils import datastructs


def trace_print_before(abs_settings=None):
    trace_debug(position=trace.POS_BEFORE, abs_settings=abs_settings)


def trace_print_after(abs_settings=None):
    trace_debug(position=trace.POS_AFTER, abs_settings=abs_settings)


def debug_print_before(abs_settings=None):
    trace_debug(position=trace.POS_BEFORE, abs_settings=abs_settings)


def debug_print_after(abs_settings=None):
    trace_debug(position=trace.POS_AFTER, abs_settings=abs_settings)


# @decorators.logging_debug_reset
def trace_debug(position=0, abs_settings=None):
    logger.debug('start')

    def _trcdeb_ident_format(ident):
        """ nested helper function for formatting individual identifiers """
        if ident:
            return ' %s=["+%s+"]' % (ident, ident)
        return ''

    def _trcdeb_trc_line(trc_ln):
        """ nested helper function for building one trace line

        Keyword arguments:
            trc_ln -- list of identifiers
        """
        return ''.join(_trcdeb_ident_format(ident=t) for t in trc_ln if t)

    e = editor.Editor()
    text = e.get_selected_text(
        rm_cont_chars=False,
        rm_eol_chars=True,
        rm_comments=True
    )
    if not text:
        text = e.get_current_line(rstrip=True, continuation=True)

    if text:
        # honor trace sort setting
        if not abs_settings:
            abs_settings = settings.get_settings()

        ident_list = parse.parse_identifiers(
            s=text,
            sort=abs_settings.TRACE_VAR_SORT,
            exclude_ashell_funcs=abs_settings.TRACE_EXCLUDE_ASHELL_FUNCS,
            exclude_func_calls=abs_settings.TRACE_EXCLUDE_FUNCS,
            exclude_proc_calls=abs_settings.TRACE_EXCLUDE_PROCS,
            exclude_func_called_as_proc=abs_settings.TRACE_EXCLUDE_FUNCS_CALLED_AS_PROCS,
        )
        logger.debug('len(ident_list): %s ' % len(ident_list))
        logger.debug('ident_list: %s ' % ident_list)
        if ident_list:

            # honor trace row count setting
            if abs_settings.TRACE_VAR_ROW_CT > 0:
                trace_list = datastructs.list_reshape(
                    n=abs_settings.TRACE_VAR_ROW_CT,
                    iterable=ident_list
                )
            else:
                trace_list = [ident_list]

            _, cur_label = e.get_current_label(with_format=True)

            trace_debug, print_pause = trace.get_type_string(
                typ=abs_settings.TRACE_DEFAUL_TYPE
            ).split('.')
            if not trace_debug:
                trace_debug = 'trace'
            if not print_pause:
                print_pause = 'print'

            # trace_list is a list of lists
            # build one trace string with embedded EOLs from trace_list
            trc_str = e.get_eol().join(['%s.%s "%s%s"' % (
                        trace_debug,
                        print_pause,
                        cur_label,
                        _trcdeb_trc_line(tl),
                    ) for tl in trace_list
                ]
            )

            if position == trace.POS_BEFORE:
                e.insert_text_before_selection(text=trc_str, eol=True)
            elif position == trace.POS_AFTER:
                e.insert_text_after_selection(text=trc_str, eol=True)
            else:
                logger.warning('invalid trace/debug insert position')
    else:
        logger.warning('no text')
    logger.debug('end')


# @decorators.logging_debug_reset
def toggle_type():
    """ toggle between trace types for selection or current line
    """

    edit_obj = editor.Editor()
    abs_settings = settings.get_settings()

    def tt(line_num):
        """ nested helper """
        logger.debug('line_num: %s' % line_num)

        line_str = edit_obj.get_line_from_linenumber(
            line_num=line_num,
            continuation=False
        )
        logger.debug('line_str: %s' % line_str)

        for typ_cod, typ_str in abs_settings.typ_strs.iteritems():
            logger.debug('typ_cod: %s typ: %s' % (typ_cod, typ_str))
            line_start = line_str[:len(typ_str.strip())].lower()
            logger.debug('line start: %s' % line_start)
            if line_start == typ_str.lower():

                line_str = line_str.replace(
                    line_str[:len(typ_str.strip())],
                    trace.get_next_type_string(typ_cod=typ_cod),
                )
                edit_obj.replace_line(line_num=line_num, text=line_str)
                break

    edit_obj.execute_for_selection_linenum(tt)
