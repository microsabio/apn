"""
Description:

Author: Stephen Funkhouser

History:
    Oct 28, 2014 - 1.0.1 - Stephen Funkhouser
        1. start work on "Structure Viewer"
    Aug 20, 2014 - 1.0.0 - Stephen Funkhouser
        Created
Notes:
"""
VERSION = "1.0.1"
__updated__ = 'Oct 29, 2014'

# always import/setup logger first
import logging
from ..logger import AsbLogger
logging.setLoggerClass(AsbLogger)
logger = logging.getLogger(AsbLogger.ASB_LOGGER_NAME)

from ...asb import lsm, structs
from ..utils import rundetached


def show_structure():
    l = lsm.Lsm()
    identifier = l.get_current_identifier()
    logger.debug('identifier: %s' % identifier)
    if identifier:
        s = l.get_variable_structure(ident=identifier)
        if s:
            structfile = structs.write_struct_file(s)
            rundetached.run_detached_structure_viewer(structfile)
        else:
            logger.error('No structure object to write: %s' % identifier)
