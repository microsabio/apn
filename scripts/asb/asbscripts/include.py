"""
Description: Open Include file routines

Author: Stephen Funkhouser

History:
    Oct 09, 2014 - 1.0.1 - Jack McGregor
        a. Adjust open_include to use includes lst file rather than tags file
Notes:
"""
VERSION = "1.0.1"


# always import/setup logger first
import logging
from ..logger import AsbLogger
logging.setLoggerClass(AsbLogger)
logger = logging.getLogger(AsbLogger.ASB_LOGGER_NAME)

import re

# relative import
from ...asb import fileasb, decorators, editor, position, tags


# @decorators.logging_debug_reset
def open_include():
    """ open ++include file (on line where cursor is) """

    edit_obj = editor.Editor()
    ln = edit_obj.get_current_line()
    # check if line contains ++include fspec
    search_obj = re.match("\s*\+\+include\S*?\s+([^ \t!]*)", ln, re.I)
    if not search_obj:
        # try again with $COPY
        search_obj = re.match("\s*\$COPY\s*([^ \t!]*)", ln, re.I)

    if search_obj:
        incspec = search_obj.group(1)  # full ++include spec as given in source
        logger.debug("Looking for include file: %s" % (incspec))

        fname = fileasb.amos_to_native_file(incspec)
        logger.info("Include file to native: %s" % fname)               # [101]
        if fname:
            # [101] t = tags.Tags()
            # [101] native_spec = t.get_filespec(fspec=fname)
            native_spec = fileasb.get_include_filepath(fspec=fname)     # [101]
            logger.info("Include file spec: [%s]" % native_spec)        # [101]
            if native_spec:
                p = position.Position()
                p.push()

                edit_obj.open_document(native_spec)
            else:
                logger.warning("Include file not found: %s" % incspec)
                logger.warning("Try re-saving/compiling main program.")
    else:
        logger.warning(
            "Command requires ++include (or $COPY) file line context"
        )
