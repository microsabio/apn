import logging
from ..logger import AsbLogger
logging.setLoggerClass(AsbLogger)
logger = logging.getLogger(AsbLogger.ASB_LOGGER_NAME)

import webbrowser

# relative import
from ...asb import editor


def search_doc():
    """ doc search for selected keyword """

    logger.debug('start')

    url = 'http://www.microsabio.net/dist/doc/conref/00ashref.htm#?search='
    try:
        edit_obj = editor.Editor()
        webbrowser.open(
            url + edit_obj.sci.GetTextRange(
                edit_obj.sci.SelectionStart,
                edit_obj.sci.SelectionEnd
            )
        )
    except:
        webbrowser.open(
            'http://www.microsabio.net/dist/doc/conref/00ashref.htm'
        )
        logger.info('no selection so just open documentation web page')
    logger.debug('end')
