"""
Description: Scripts relating to commenting/uncommenting lines

Author: Stephen Funkhouser

History:
    Nov 30, 2014 - 1.0.1 - Jack McGrgor
        1. Move cursor to next line after comment_toggle_block
Notes:
"""
VERSION = "1.0.1"

import logging
from ..logger import AsbLogger
logging.setLoggerClass(AsbLogger)
logger = logging.getLogger(AsbLogger.ASB_LOGGER_NAME)

# relative import
from ...asb import editor, settings


def comment_toggle_block():
    """ block comment/uncomment """
    edit_obj = editor.Editor()

    def bct(line_num):
        marker = "!>!"
        edit_obj.sci.GotoLine(line_num)
        curpos = edit_obj.sci.CurrentPos
        match = edit_obj.sci.GetTextRange(curpos, curpos + 3)
        if match == marker:
            edit_obj.sci.CurrentPos += 3
            edit_obj.sci.DelLineLeft()
        else:
            edit_obj.sci.AddText(3, marker)

    edit_obj.execute_for_selection_linenum(bct)
    # [101] At this point, if we had selected lines, they would still be selected, and
    # [101] the cursor would be somewhere on the last line modified. Leaving it here allows
    # [101] the toggle command to be repeated (i.e. reversed). But since ^Z (undo) also does
    # [101] that, the following behavior (proposed by Jorge) moves the cursor to the start of
    # [101] the next line, allowing successive ^Q commands to toggle successive lines.
    edit_obj.sci.LineEnd()            # [101] move to end of line
    edit_obj.sci.CurrentPos += 1      # [101] start of next line
    edit_obj.sci.SetSelection(edit_obj.sci.CurrentPos,edit_obj.sci.CurrentPos)  # [101] remove selection
    
def comment_end_of_line():
    edit_obj = editor.Editor()
    abs_settings = settings.get_settings()

    def ceol(line_num):
        """ Nested helper to handle individual lines
        """

        lnstr = edit_obj.sci.GetLine(line_num)
        logger.debug('lnstr: %s' % lnstr)

        # if line starts with a comment skip it
        if lnstr.startswith('!'):
            return

        # get position of trailing comment if one exists
        quote_ct = 0
        comment_pos = 0
        for i, c in enumerate(lnstr):
            if c == '"':
                quote_ct += 1

            if c == '!':
                if quote_ct % 2 == 0:
                    comment_pos = i
                    # [116] break on first unquoted comment character /smf
                    break
        logger.debug('comment_pos: %s' % comment_pos)

        trail_text = ''
        real_text = ''
        if comment_pos > 0:
            trail_text = lnstr[comment_pos + 1:].strip()
            real_text = lnstr[:comment_pos].rstrip()
        else:
            real_text = lnstr.rstrip()

        #======================================================================
        # Delete last comment and trail text, or just ensure we strip
        # trailing spaces from text
        #======================================================================
        edit_obj.sci.CurrentPos = edit_obj.sci.PositionFromLine(line_num)
        edit_obj.sci.LineDelete()

        real_text += edit_obj.get_eol()
        edit_obj.sci.AddText(len(real_text), real_text)

        logger.debug('trail_text: %s' % trail_text)
        logger.debug('real_text: %s' % real_text)

        # set current position to the end of line
        edit_obj.sci.CurrentPos = edit_obj.sci.GetLineEndPosition(line_num)

        # calculate line length without end of line chars
        line_length = edit_obj.sci.GetLineEndPosition(line_num) - \
            edit_obj.sci.PositionFromLine(line_num)
        if line_length <= abs_settings.COMMENT_EOL_POS:
            eol_pos = abs_settings.COMMENT_EOL_POS
        elif line_length <= abs_settings.COMMENT_EOL_POS_WIDE:
            eol_pos = abs_settings.COMMENT_EOL_POS_WIDE
        else:
            eol_pos = 0

        if eol_pos > 0:
            # temporarily make line long enough to allow FindColumn() to work
            edit_obj.sci.AddText(eol_pos, ' ' * eol_pos)
            # use FindColumn because it will take Tabs into account
            edit_obj.sci.CurrentPos = edit_obj.sci.FindColumn(
                line_num, eol_pos - 1
            )
        else:
            edit_obj.sci.AddText(5, ' ' * 5)
        # add
        edit_obj.sci.AddText(2, '! ')

        if trail_text:
            # add any trailing text
            edit_obj.sci.AddText(len(trail_text), trail_text)

        # delete any characters to the right of current pos to the end of line
        edit_obj.sci.DelLineRight()

    edit_obj.execute_for_selection_linenum(ceol)
