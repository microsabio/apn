# -*- mode: python -*-
a = Analysis(['D:\\vm\\APN\\scripts\\asb\\pyqtwidgets\\structure_viewer.py'],
             pathex=['D:\\vm\\APN\\scripts\\asb\\pyqt\\'],
             hiddenimports=[],
             hookspath=None,
             runtime_hooks=None)
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='structure_viewer.exe',
          debug=False,
          strip=None,
          upx=True,
          console=True )
          
css = Tree('D:\\vm\\APN\\scripts\\asb\\pyqt\\static\\css\\', prefix='css') 

coll = COLLECT(exe,
               a.binaries,
               css,
               a.zipfiles,
               a.datas,
               strip=None,
               upx=True,
               name='structure_viewer')
