"""
Description:

Author: Stephen Funkhouser

History:
    Jan 29, 2015 - 1.0.3 - Jack McGregor
        1. Remove warning about project list not found if no project
    Aug 27, 2014 - 1.0.2 - Jack McGregor
        1. __set_current_project_group() to re-determine project group to
            fix problem where project group changed and we don't know it
    Aug 13, 2014 - 1.0.1 - Stephen Funkhouser
        1. __set_current_project_group() - add fall back to 'ppg' when
           there's no current project group.
    Jul 25, 2014 - 1.0.0 - Stephen Funkhouser
        Created
Notes:
"""
VERSION = "1.0.3"

# always import/setup logger first
import logging
from .logger import AsbLogger
logging.setLoggerClass(AsbLogger)
logger = logging.getLogger(AsbLogger.ASB_LOGGER_NAME)

import os
import pn
from xml.dom import minidom


class Projects(object):

    # note this is a class attribute, not instance
    __current_project_group = ''

    def __init__(self):
        """ nothing for now
        """

        self.__set_current_project_group()

    def get_include_list_files(self):
        """ return a list of valid include files sorted from newest - oldest

        returns:
            list - list of include files
        """

        il_dir = self.get_include_list_dir()
        logger.debug('il_dir: %s exists? %r' % (
                il_dir, os.path.exists(il_dir)
            )
        )
        if os.path.exists(il_dir):
            mtime = lambda f: os.stat(os.path.join(il_dir, f)).st_mtime
            # use reversed to get newest - oldest
            include_flist = [
                os.path.join(il_dir, inc) for inc in os.listdir(il_dir)
            ]
            include_flist.sort(key=mtime, reverse=True)
            return include_flist
        return []

    def get_include_list_dir(self):
        """ Get the directory for include list files created by tagger

        %TEMP%\APN\<project_group>\includes\
        """

        return os.path.join(
            self.get_temp_project_group_dir(),
            'includes',
        )

    def get_current_project_group(self):
        if not Projects.__current_project_group:
            self.__set_current_project_group()
        return Projects.__current_project_group

#    @decorators.logging_debug_reset
    def __set_current_project_group(self):
        """ For now all we have is curproj.lst to get this context from
        """

        logger.debug('start Projects.__current_project_group: %s' %
            Projects.__current_project_group
        )
        Projects.__current_project_group = None     # force re-calc of project
        
        if not Projects.__current_project_group:
            proj_grp_fpath = self.get_curproj_attribute('ProjGroupFile')
            logger.debug('proj_grp_fpath: %s' % proj_grp_fpath)
            if proj_grp_fpath:
                if os.path.exists(proj_grp_fpath):
                    logger.debug('project group file exists!')
                    xmldoc = minidom.parse(proj_grp_fpath)
                    # returns a list if tag name found
                    workspace = xmldoc.getElementsByTagName('Workspace')
                    logger.debug('workspace: %s' % (workspace))
                    if workspace:
                        Projects.__current_project_group = str(
                            workspace[0].getAttribute('name')
                        )
                        logger.debug('proj_grp: %s' %
                            Projects.__current_project_group
                        )
                else:
                    logger.warning('Project Group file not found!')
            else:
                # assume fall back project group of 'ppg'
                logger.debug("assume fall back project group of 'ppg'")
                Projects.__current_project_group = 'ppg'
        else:
            logger.debug('already set')

    def get_temp_project_group_dir(self):
        """ Get the directory for include list files created by tagger

        %TEMP%\APN\<pid>\<project_group>\
        """

        return os.path.expandvars(
            os.path.join(
                '%TEMP%',
                'APN',
                str(pn.PID()),
                self.get_current_project_group(),
            )
        )

    def get_curproj_attribute(self, attr):
        curproj_fpath = self.__get_curproj_filepath()
        logger.debug('curproj_fpath: %s' % curproj_fpath)

        if os.path.exists(curproj_fpath):
            with open(curproj_fpath, 'r') as f:
                for ln in f:
                    ln = ln.strip()
                    if ln.startswith('ProjGroupFile'):
                        return ln.split('=')[1]
        elif Projects.__current_project_group:    # [1.0.3]
            logger.warning('Current Project list file not found!')

    def __get_curproj_filepath(self):
        """
        %TEMP%\APN\<pid>\<project_group>\
        """

        return os.path.expandvars(
            os.path.join(
                '%TEMP%',
                'APN',
                str(pn.PID()),
                'curproj.lst',
            )
        )
