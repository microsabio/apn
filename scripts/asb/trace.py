"""
Description: Trace related functions

Author: Stephen Funkhouser

History:
    Aug 15, 2014 - 1.0.1 - Stephen Funkhouser
        add get_next_type_string()
    Aug 14, 2014 - 1.0.0 - Stephen Funkhouser
        Created
Notes:
"""
VERSION = "1.0.1"

# always import/setup logger first
import logging
from .logger import AsbLogger
logging.setLoggerClass(AsbLogger)
logger = logging.getLogger(AsbLogger.ASB_LOGGER_NAME)

import asbscripts
from ..asb import settings

POS_BEFORE = 1
POS_AFTER = 2


def get_script_title(position, exclude_func_match_settings=True):
    """ Get script title
    """
    abs_settings = settings.get_settings()

    if position == POS_BEFORE:
        pos_str = '(Before)'
    elif position == POS_AFTER:
        pos_str = '(After)'
    else:
        pos_str = 'Invalid'
        logger.warning('invalid trace/debug insert position')

    if exclude_func_match_settings:
        if abs_settings.TRACE_EXCLUDE_FUNCS:
            func_str = 'Return Funcs Excluded'
        else:
            func_str = 'Return Funcs Included'
    else:
        if abs_settings.TRACE_EXCLUDE_FUNCS:
            func_str = 'Return Funcs Included - Opposite of settings'
        else:
            func_str = 'Return Funcs Excluded - Opposite of settings'

    return '{typ: <11} {pos: <8} {func}'.format(
        typ=get_type_string(abs_settings.TRACE_DEFAUL_TYPE),
        pos=pos_str,
        func=func_str,
    )


def get_type_string(typ):
    """ Get type string associated with a type code
    """

    if typ in settings.Settings().typ_strs:
        return settings.Settings().typ_strs[typ]
    else:
        logger.warning('invalid trace/debug type')
        return 'Invalid Type'


def get_next_type_string(typ_cod):
    """
    """
    logger.debug('start: typ_cod: %s' % typ_cod)
    typ_cod += 1
    # type codes start at zero so we need to subtract 1 from len for test
    if typ_cod > len(settings.Settings().typ_strs) - 1:
        typ_cod = 0
    logger.debug('return: typ_cod: %s string: %s' % (
            typ_cod,
            settings.Settings().typ_strs[typ_cod]
        )
    )
    return settings.Settings().typ_strs[typ_cod]


def call(position=POS_AFTER, exclude_func_match_settings=True):
    """ Call trace_debug
    """
    if exclude_func_match_settings:
        asbscripts.tracedebug.trace_debug(position=position)
    else:
        abs_settings = settings.get_settings()
        # toggle boolean
        abs_settings.TRACE_EXCLUDE_FUNCS = not abs_settings.TRACE_EXCLUDE_FUNCS
        asbscripts.tracedebug.trace_debug(
            position=position,
            abs_settings=abs_settings
        )
