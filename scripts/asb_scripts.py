"""
History:
    Feb 02, 2015 - 1.0.6 - Add "(F1)" shortcut to doc search
    Aug 16, 2014 - 1.0.5 - Stephen Funkhouser
        1.  Setup current default indenter
    Aug 15, 2014 - 1.0.4 - Stephen Funkhouser
        1.  call scheme.configure_glue() to start process of using events
    Aug 15, 2014 - 1.0.3 - Stephen Funkhouser
        1. add trace_type_toggle()
    Aug 14, 2014 - 1.0.2 - Stephen Funkhouser
        1. change trace scripts to only include before/after
           scripts for users default setting type.
        2. includes traces for including/excluding function returns
           identifiers.
           the top most scripts are based on the users settings
    Aug 13, 2014 - 1.0.1 - Jack McGregor
        1. add case_toggle()
Created on Jul 23, 2014

@author: Stephen Funkhouser
"""
VERSION = "1.0.6"

# always import/setup logger first
import logging
from asb.logger import AsbLogger
# setup named custom logger
logging.setLoggerClass(AsbLogger)
logger = logging.getLogger(AsbLogger.ASB_LOGGER_NAME)
# uncomment and change the following to affect log level for our logger
# logger.setLevel(logging.DEBUG)
# uncomment and set True to enable APN output
logger.set_log_pn_output(True)

from pypn import decorators as pypndecorators

from asb import asbscripts, indenter, scheme, trace

# configure glue events for A-Shell Basic scheme
scheme.configure_glue()


@pypndecorators.indenter("asb")
def asb_indent(c, doc):
    """ Call indenter

        get_indenter() will return the appropriate indenter object
    """
    indenter.get_indenter()(c=c, doc=doc)

#==============================================================================
# Script callers go here
#==============================================================================


@pypndecorators.script("Go To Definition (^alt+G)", "ASB")
def goto_identifier_definition():
    """ goto definition of selected identifier """
    asbscripts.definition.goto_identifier_definition()


@pypndecorators.script("Show Structure (^alt+S)", "ASB")
def show_structure():
    """ goto definition of selected identifier """
    asbscripts.showstructure.show_structure()


@pypndecorators.script("Go Back (^alt+B)", "ASB")
def go_back_position():
    """ return to previous location """
    asbscripts.pos.go_back()


@pypndecorators.script("Go Forward (^alt+F)", "ASB")
def go_fwd_position():
    """ return to next location """
    asbscripts.pos.go_forward()


@pypndecorators.script("Push Position (^alt+P)", "ASB")
def push_position():
    """ save current file, position """
    asbscripts.pos.position_push()


@pypndecorators.script("Position Clear", "ASB")
def clear_position():
    """ clear all saved file, positions """
    asbscripts.pos.clear()


@pypndecorators.script("Block Comment Toggle (^Q)", "ASB")
def block_comment_toggle():
    """ block comment/uncomment """
    asbscripts.comment.comment_toggle_block()


@pypndecorators.script("Comment End of Line (^shift+1)", "ASB")
def comment_end_of_line():
    """ add comment to end of line for selection. Moves trailing comment to
        column position defined in settings
    """
    asbscripts.comment.comment_end_of_line()


@pypndecorators.script("Doc Search (F1)", "ASB")
def doc_search():
    """ doc search for selected keyword """
    asbscripts.searchdoc.search_doc()


@pypndecorators.script("Open ++include (^alt+O)", "ASB")
def open_include():
    """ Open ++include file referenced on current line
    """
    asbscripts.include.open_include()


@pypndecorators.script(trace.get_script_title(position=trace.POS_AFTER), "ASB Tracing")
def trace_after():
    """  """
    trace.call(position=trace.POS_AFTER)


@pypndecorators.script(trace.get_script_title(position=trace.POS_BEFORE), "ASB Tracing")
def trace_before():
    """  """
    trace.call(position=trace.POS_BEFORE)


@pypndecorators.script(trace.get_script_title(
        position=trace.POS_AFTER,
        exclude_func_match_settings=False
    ), "ASB Tracing"
)
def trace_after_func_opposite():
    """  """
    trace.call(position=trace.POS_AFTER, exclude_func_match_settings=False)


@pypndecorators.script(trace.get_script_title(
        position=trace.POS_BEFORE,
        exclude_func_match_settings=False
    ), "ASB Tracing"
)
def trace_before_func_opposite():
    """  """
    trace.call(position=trace.POS_BEFORE, exclude_func_match_settings=False)


@pypndecorators.script("Toggle Trace Type", "ASB Tracing")
def trace_type_toggle():
    """ """
    asbscripts.tracedebug.toggle_type()


@pypndecorators.script("Go To Top (^HOME)", "ASB Editing Wrappers")
def goto_top():
    """ go to top of doc (wrapper for ^HOME to save pos for go-back) """
    asbscripts.pos.go_top()


@pypndecorators.script("Go To Bottom (^END)", "ASB Editing Wrappers")
def goto_bottom():
    """ go to bottom of doc (wrapper for ^HOME to save pos for go-back) """
    asbscripts.pos.go_bottom()


@pypndecorators.script("Beautify", "Xml")
def beautify_xml():
    asbscripts.xmlasb.beautify()


@pypndecorators.script("Validate", "Xml")
def validate_xml():
    asbscripts.xmlasb.validate()


@pypndecorators.script("Toggle Case (alt+UP)", "Text")
def case_toggle():
    """ toggle case """
    asbscripts.case.toggle()
