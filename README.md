# A-Shell Programmer's Notepad (APN) #

A modern, Windows integrated editor/compiler for A-Shell BASIC (ASB)

## Overview / Features ##

* Built on Simon Steele's Programmer's Notepad 2.4 (thanks Simon!)
* Understands ASB (syntax highlighting, ppns, ersatzes, auto-indent, etc.)
* Integrated compiler
* Project organizer
* Extensible using scripts written in Python
* ASB-specific script functions, including:
* * Open ++include module (in new window/panel)
* * Goto definition of variable or function (opening target file as needed)
* * Show structure (including member offsets)
* * Search ASB documentation for highlighted token
* * Generate/insert trace/debug statements
* * Block comment/uncomment
* Object tag viewer (view/goto constants, structures, variables, functions, modules, etc.)
* Text clip (macro) facility
* Ability to integrate external tools/commands

## How to get started? ##

See the [APN Wiki](https://bitbucket.org/microsabio/apn/wiki/Home)

## Support ##

Contact MicroSabio for details.

