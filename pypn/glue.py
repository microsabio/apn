######################################################
# # This stuff is all essential for pypn to work - that
# # means don't mess with it junior!

# # That being said, here are the APN changes...
# # [100] 05-Jul-14 Add onDocClose() /jdm
# # [101] 15-Aug-14 Update onDocLoad() so it actually works /smf
# # [102] 15-Aug-14 Update onCharAdded() handle end of line chars
# #                 to properly for calling indenter
# # [103] 02-Feb-16 import tchelpers

import pn, debug, scintilla, record
import string
import sys
import os

# [103] import tchelpers.py if present (typically from scripts directory)
try:
	import tchelpers
except:
	pass

schemes = {}
scripts = {}
oldstdout = None


class SchemeMapping:
    def __init__(self, name):
        self.name = name
        self.on_char_added = None
        self.indenter = None
        self.on_doc_load = None


class StdOutCapture:
    """ Simple output capturer """
    def __init__(self):
        self.output = ""

    def write(self, s):
        self.output = self.output + s

    def getOutput(self):
        return self.output


def registerScript(f, group, scriptName):
    """ This is called by the script decorator to register
    a script. """
    scripts[f.func_name] = f
    pn.RegisterScript(f.func_name, group, scriptName)


def runScript(name):
    """ This is called by PN to run a script by name. """
    try:
        scripts[name]()
    except KeyError:
        pass


def onCharAdded(c, doc):
    """ Method called when a character is added, default behavior
        manages calling indenters and also calls any method
        registered with glue.schemes[scheme].on_char_added
    """

    if not doc.CurrentScheme in schemes:
        return
    scheme = schemes[doc.CurrentScheme]
    if scheme and scheme.on_char_added:
        scheme.on_char_added(c, doc)
    if scheme and scheme.indenter:
        #======================================================================
        # Handle Window/Linux end of line for indenter
        # Window's this gets called twice because it's 2 char EOL
        # Linux it gets called just once
        #
        # mode = 0 -> '\r\n' window's
        # mode = 1 -> '\r'
        # mode = 2 -> '\n'
        #======================================================================
        eol_mode = scintilla.Scintilla(doc).EOLMode
        if eol_mode == 0 and c == '\r':
            return
        elif not (c == '\n' or c == '\r'):
            return
        scheme.indenter(c, doc)


def onDocLoad(doc):
    """ Method called when a document is loaded into PN """
    if doc.CurrentScheme in schemes:
        scheme = schemes[doc.CurrentScheme]
        scheme.on_doc_load(doc)


def onDocSave(filename, doc):
    """ Method called when a document is about to be saved to a file """
    pass


def onDocSaved(doc):
    """ Method called when a document has been saved """
    pass


def onDocClose(doc):
    """ Method called when a document is being closed [jm] """
    pass


def onModifiedChanged(modified, doc):
    """ Method called when the modified flag is changed """
    pass


def onWriteProtectChanged(protected, doc):
    """ Method called when the read only status of a document is changed """
    pass


def getSchemeConfig(name):
    """ Get a pypn scheme configuration, used to hook up indenter
    functions and character added handlers """
    if not name in schemes:
        schemes[name] = SchemeMapping(name)
    return schemes[name]


def startRecording():
    """ PN calls this to start recording a script, all recording actions
    are delegated to the Recorder class """
    return record.Recorder()


def startCapturingStdOut():
    """ PN calls this to start capturing stdout """
    global oldstdout
    oldstdout = sys.stdout
    sys.stdout = StdOutCapture()


def finishCapturingStdOut():
    """ PN calls this to finish stdout capture and get the output. """
    global oldstdout

    if (oldstdout != None):
        ret = sys.stdout.getOutput()
        sys.stdout = oldstdout
        oldstdout = None
        ret = string.rstrip(ret, "\r\n")
        return ret
    else:
        return ""


def evalScript(script):
    script = string.replace(script, "\r\n", "\n")
    script = string.replace(script, "\r", "\n")
    exec(script)
    return ""


def evalCommand():
    pass


def evalCommandEnter():
    pass

