<?xml version="1.0" encoding="utf-8"?>

<!-- API definitions for SOSFUNC: functions (and some SOSLIB SBXs) 
     Notes:
        - To activate: save as %APN%\settings\asb.api
        - This API is appended to the one defined in %APN%\schemes\asb.api
        - See Tools > Autocomplete to enable/config auto-complete (Ctrl+space for manual)
        - File must start with the <?xml header and then be wrapped in <AutoComplete> .. </AutoComplete>
        - Individual functions/sbxs defined with <KeyWord name=> <Overload retVal=> ... </Overload></KeyWord> blocks
        - If retVal starts with "[<file.ext>]" it will display on a separate line (useful for identifyng file where funct defined) 
        - Standalone keywords (for autocomplete but not call tips) can be defined with <KeyWord name="xxx", />
        - Routine names not case sensitive (as of 2.4.0.14)
        - Names will appear in auto-complete alphabetized  
        - Param names can include descriptive prefix or suffix strings (avoid commas, parens)
        - Max # routines is 1024 (including those in schemes\asb.api)
-->

<AutoComplete>

  <!-- standalone keyword -->
  <KeyWord name="Zzyzx" />
  
 <!-- SBX routines (use func="no", retVal="sbx") -->
 <!-- Format is same as for internal SBRs (see schemes\asb.api for examples)  -->

  <!-- XCALL ATEPRT,FILE{,PRINTER,SWITCHES,COPIES} -->
  <KeyWord name="ATEPRT" func="no">
    <Overload retVal="sbx">
        <Param name="file$ [in]" />
        <Param name="{printer [in]" />
        <Param name="{switches [in]" />
        <Param name="{copies [in]}}}" />
    </Overload>	
  </KeyWord>    
 
  <!-- XCALL ATEREG,OP,STATUS,VALUENAME$,VALUEDATA -->
  <KeyWord name="ATEREG" func="no">
    <Overload retVal="sbx">
        <Param name="opcode [in: 0=get 1=set]" />
        <Param name="status [out: >=0=success]" />
        <Param name="valuename$ [in]" />
        <Param name="valuedata$ [in/out]" />
    </Overload>	
  </KeyWord>    
 
  <!-- XCALL ATESYC,CLINE$,STATUS -->
  <KeyWord name="ATESYC" func="no">
    <Overload retVal="sbx">
        <Param name="cline$ [in]" />
        <Param name="status [out]" />
    </Overload>	
  </KeyWord>    

 <!-- SOSFUNC functions (use func="yes", retVal="s0", "f6", "num", "string", etc. ) -->
  
  <!-- Fn'ANSI'to'UTF8$(ansi$:inputonly as s0) as s0  -->
  <KeyWord name="Fn'ANSI'to'UTF8$" func="yes">
    <Overload retVal="[sosfunc:fnansiutf.bsi] s0">
      <Param name="ansitext$ as s0:in" />
    </Overload>	
  </KeyWord>      

  <!-- Fn'ATE'SBX(sbxname$ as s12:inputonly, arg1$ as s0:inputonly, arg2$ as s0:inputonly, &
                    arg3$ as s0:inputonly, arg4$ as s0:inputonly, arg5$ as s0:inputonly, &
                    arg6$ as s0:inputonly, arg7$ as s0:inputonly, arg8$ as s0:inputonly, &
                    arg9$ as s0:inputonly) as i4  -->
  <KeyWord name="Fn'ATE'SBX" func="yes">
    <Overload retVal="[sosfunc:fnatesbx.bsi] i4">
      <Param name="arg1$ as s0:in" />
      <Param name="..." />
      <Param name="arg9$ as s0:in" />
    </Overload>	
  </KeyWord>      
  
  <!-- Fn'aui'name2id(ctlname$ as s24:inputonly) as b2 -->
  <KeyWord name="Fn'aui'name2id" func="yes">
    <Overload retVal="[sosfunc:fnname2id.bsi] b2">
      <Param name="ctlname$:in" />
    </Overload>	
  </KeyWord>      

  <!-- Fn'aui'id2name$(id as b2:inputonly) as s24 -->
  <KeyWord name="Fn'aui'id2name" func="yes">
    <Overload retVal="[sosfunc:fnname2id.bsi] s24">
      <Param name="id as b2:in" />
    </Overload>	
  </KeyWord>      

  <!-- Fn'B64Inc$(b64$ as s20:inputonly) as s20 -->
  <KeyWord name="Fn'B64Inc$" func="yes">
    <Overload retVal="[sosfunc:fndecb64.bsi] s20">
      <Param name="b64$ as s20:in" />
    </Overload>	
  </KeyWord>      
  
  <!-- Fn'B64ToDec(b64$ as s20) as f8 - convert b64 string to numeric value -->
  <KeyWord name="Fn'B64ToDec" func="yes">
    <Overload retVal="[sosfunc:fndecb64.bsi] f8">
      <Param name="b64$ as s20:in" />
    </Overload>	
  </KeyWord>      
  
   <!-- Fn'BitClr$(bitfield as x0:inputonly, bitno as b2:inputonly) as x0 -->
  <KeyWord name="Fn'BitClr$" func="yes">
    <Overload retVal="[sosfunc:fnbits.bsi] x0">
      <Param name="bitfield as x0:in" />
      <Param name="bitno as b2:in" />
    </Overload>	
  </KeyWord>      

   <!-- Fn'BitSet$(bitfield as x0:inputonly, bitno as b2:inputonly) as x0 -->
  <KeyWord name="Fn'BitSet$" func="yes">
    <Overload retVal="[sosfunc:fnbits.bsi] x0">
      <Param name="bitfield as x0:in" />
      <Param name="bitno as b2:in" />
    </Overload>	
  </KeyWord>      

  <!-- Fn'BitToggle$(bitfield as x0:inputonly, bitno as b2:inputonly) as x0 -->
  <KeyWord name="Fn'BitToggle$" func="yes">
    <Overload retVal="[sosfunc:fnbits.bsi] x0">
      <Param name="bitfield as x0:in" />
      <Param name="bitno as b2:in" />
    </Overload>	
  </KeyWord>      

  <!-- Fn'BitIsSet(bitfield as x0:inputonly, bitno as b2:inputonly) as i2 -->
  <KeyWord name="Fn'BitIsSet" func="yes">
    <Overload retVal="[sosfunc:fnbits.bsi] i2">
      <Param name="bitfield as x0:in" />
      <Param name="bitno as b2:in" />
    </Overload>	
  </KeyWord>      

  <!-- fn'chartype(char as s1) as i2 -->
  <KeyWord name="fn'chartype" func="yes">
    <Overload retVal="[sosfunc:fnistype.bsi] i2">
      <Param name="char as s1:inputonly" />
    </Overload>	
  </KeyWord>      

  <!-- Function Fn'DateTimeOffset$(idate as b4:inputonly, itime as b4:inputonly, &
                            offdays as f6:inputonly, offhrs as f6:inputonly, &
                            odate as b4:outputonly, otime as f6:outputonly, &
                            flags as f6:inputonly) as s50 -->
  <KeyWord name="Fn'DateTimeOffset$" func="yes">
    <Overload retVal="[sosfunc:fndtoffset.bsi] s50">
      <Param name="idate as b4:in" />
      <Param name="itime as b4:in" />
      <Param name="offdays as f6:in" />
      <Param name="offhrs as f6:in" />
      <Param name="odate as b4:out" />      
      <Param name="otime as f6:out" />      
      <Param name="flags as f6:in" />            
    </Overload>	
  </KeyWord>      

  <!-- Fn'DecToB64$(num as f8:inputonly, digits as b1:inputonly) as s20 -->
  <KeyWord name="Fn'DecToB64$" func="yes">
    <Overload retVal="[sosfunc:fndecb64.bsi] s20">
      <Param name="decval as f8:in" />
      <Param name="digits as b1:in" />
    </Overload>	
  </KeyWord>      
  
  <!-- Fn'Dec2Hex$(decval as f6:inputonly,flags as b2:inputonly,width as b2:inputonly) as s16 -->
  <KeyWord name="Fn'Dec2Hex$" func="yes">
    <Overload retVal="[sosfunc:fndec2hex.bsi] s16">
      <Param name="decval as f6:in" />
      <Param name="{flags as b2:in" />
      <Param name="{width as b2:in}}" />      
    </Overload>	
  </KeyWord>      

  <!-- Fn'Detect'ZTERM'ATE$() as s12 -->
  <KeyWord name="Fn'Detect'ZTERM'ATE$" func="yes">
    <Overload retVal="[sosfunc:fnatechk.bsi] s12">
    </Overload>	
  </KeyWord>      
    
  <!-- fn'directory_exp'env$(path$ as s260:inputonly, mode as b1:inputonly) as s260 -->
  <KeyWord name="fn'directory_exp'env$" func="yes">
    <Overload retVal="[sosfunc:fnexpenv.bsi] f6">
      <Param name="path$ as s260:in" />
      <Param name="mode as b1:in [0=local 1=ATE]" />
    </Overload>	
  </KeyWord>      
  
  <!-- Fn'Explode(string$ as s0:inputonly, flags as b2, tok1$:outputonly, .... tokN$:outputonly) as i2 -->
  <KeyWord name="Fn'Explode" func="yes">
    <Overload retVal="[sosfunc:fnexplode.bsi] i2">
      <Param name="string$$ [in]" />
      <Param name="flags as b2:in/out [0=normal 1=cont]" />
      <Param name="tok1$ as s0:out" />
      <Param name="... " />
      <Param name="tokN$ as s0:out" />
    </Overload>	
  </KeyWord>      
  
  <!-- Fn'FileAge(fspec$ as s260:inputonly, flags as b4:inputonly) as i4 -->
  <KeyWord name="Fn'FileAge" func="yes">
    <Overload retVal="[sosfunc:fnfileage.bsi] i4">
      <Param name="fspec$ as s260:in" />
      <Param name="flags as b4:in [0=mtime 1=ctime +2=ATE]" />
    </Overload>	
  </KeyWord>      
  
  <!-- Fn'FileToStr$(fspec$ as s260:inputonly) as x0 -->
  <KeyWord name="Fn'FileToStr$" func="yes">
    <Overload retVal="[sosfunc:fnfilestr.bsi] x0">
      <Param name="fspec$ as s260:in" />
    </Overload>	
  </KeyWord>      
  
  <!-- Fn'FQFS$(spec$ as s256:inputonly) as s256 -->
  <KeyWord name="Fn'FQFS$" func="yes">
    <Overload retVal="[sosfunc:fnfqfs.bsi] s256">
      <Param name="fspec$ as s260:in" />
    </Overload>	
  </KeyWord>      
  
  <!-- Fn'Hex2Dec(hex$ as s16:inputonly, flags as b2:inputonly) as f -->
  <KeyWord name="Fn'Hex2Dec" func="yes">
    <Overload retVal="[sosfunc:fnhex2dec.bsi] f6">
      <Param name="hex$ as s16:in" />
      <Param name="{flags as b2:in}" />
    </Overload>	
  </KeyWord>      
  
  <!-- Fn'InputRegex(ch as b2:inputonly, pattern$ as s512:inputonly, flags as b4:inputonly, & 
                        match$ as s0:outputonly) as i4 -->
  <KeyWord name="Fn'InputRegex" func="yes">
    <Overload retVal="[sosfunc:fninpregex.bsi] i4">
      <Param name="ch as b2:in" />
      <Param name="pattern$ as s512:in" />
      <Param name="flags as b4:in" />
      <Param name="match$ as s0:out" />
      <Param name="{submatch1$ as s0:out" />
      <Param name="..." />
      <Param name="submatchN$ as s0:out" />
    </Overload>	
  </KeyWord>      
             
  <!-- Fn'Instr(spos as i2, string$ as s0, pattern$ as s0,  &
                    flags as b4) as i2 -->
  <KeyWord name="Fn'Instr" func="yes">
    <Overload retVal="[sosfunc:fninstr.bsi] i2">
      <Param name="spos as i2:in" />
      <Param name="string$ as s0:in" />
      <Param name="pattern$ as s0:in" />
      <Param name="{flags as b4:in}" />
    </Overload>	
  </KeyWord>      
           

  <!-- fn'isalpha(char as s1) as i2 -->
  <KeyWord name="fn'isalpha" func="yes">
    <Overload retVal="[sosfunc:fnistype.bsi] bool">
      <Param name="char as s1:in" />
    </Overload>	
  </KeyWord>      

  <!-- fn'isalnum(char as s1) as i2 -->
  <KeyWord name="fn'isalnum" func="yes">
    <Overload retVal="[sosfunc:fnistype.bsi] bool">
      <Param name="char as s1:in" />
    </Overload>	
  </KeyWord>      

  <!-- fn'isdigit(char as s1) as i2 -->
  <KeyWord name="fn'isdigit" func="yes">
    <Overload retVal="[sosfunc:fnistype.bsi] bool">
      <Param name="char as s1:in" />
    </Overload>	
  </KeyWord>      
  
  <!-- fn'ispunc(char as s1) as i2 -->
  <KeyWord name="fn'ispunc" func="yes">
    <Overload retVal="[sosfunc:fnistype.bsi] bool">
      <Param name="char as s1:in" />
    </Overload>	
  </KeyWord>      

  <!-- fn'is'all'digits(string$ as s0) as i2 -->
  <KeyWord name="fn'all'digits" func="yes">
    <Overload retVal="[sosfunc:fnistype.bsi] bool">
      <Param name="string$ as s0:in" />
    </Overload>	
  </KeyWord>      

  <!-- Fn'LogArchive(logfile$ as s260:inputonly, &
				maxsize as f6:inputonly, maxgen as b2:inputonly) as i2 -->
  <KeyWord name="Fn'LogArchive" func="yes">
    <Overload retVal="[sosfunc:fnlogarg.bsi] i2">
      <Param name="logfile$ as s260:in" />
      <Param name="maxsize as f6:in" />
      <Param name="maxgen as b2:in" />
    </Overload>	
  </KeyWord>      
              
  <!-- Fn'MachineName$() as s20 -->
  <KeyWord name="Fn'MachineName$" func="yes">
    <Overload retVal="[sosfunc:fnmachname.bsi] s20">
    </Overload>	
  </KeyWord>      
  
  <!-- Fn'MinAshVer(vmajor,vminor,vedit,vpatch,ateflag) as f6 -->
  <KeyWord name="Fn'MinAshVer" func="yes">
    <Overload retVal="[sosfunc:fnminasver.bsi] f6">
      <Param name="vmajor as b1:in" />
      <Param name="vminor as b1:in" />
      <Param name="vedit as b2:in" />
      <Param name="vpatch as b1:in" />
      <Param name="ateflag  as b1:in [1=ate]" />
    </Overload>	
  </KeyWord>      

  <!-- Fn'Name'Ext$(fspec$ as s260:inputonly) as s260 -->
  <KeyWord name="Fn'Name'Ext$" func="yes">
    <Overload retVal="[sosfunc:fnameext.bsi] s260">
      <Param name="fspec$ as s260:in" />
    </Overload>	
  </KeyWord>      
  
  <!-- Fn'NextCh(ch as b4:inputonly) as b4 -->
  <KeyWord name="Fn'NextCh" func="yes">
    <Overload retVal="[sosfunc:fnextch.bsi] b4">
      <Param name="startch as b4:in" />
    </Overload>	
  </KeyWord>      
  
  <!-- Fn'NumInRangeList(rangelist$ as s MAX_FN_RANGELIST:inputonly, &
                            testval as f6:inputonly) as i2 -->
  <KeyWord name="Fn'NumInRangeList" func="yes">
    <Overload retVal="[sosfunc:fnselrange.bsi] bool">
      <Param name="rangelist$ as s256:in" />
      <Param name="testval as f6:in" />
    </Overload>	
  </KeyWord>      
           
  <!-- Fn'PPN'to'Ersatz$(devppn$ as s20:inputonly, &
                          p as b2:inputonly, &
                          pn as b2:inputonly) as s16 -->
  <KeyWord name="Fn'PPN'to'Ersatz$" func="yes">
    <Overload retVal="[sosfunc:fnppn2erz.bsi] s16">
      <Param name="devppn$ as s20:in" />
      <Param name="{p as b2:in" />
      <Param name="pn as b2:in}" />
    </Overload>	
  </KeyWord>      
                 
  <!-- Fn'Print'Http'Error'Msg$(status as f6:inputonly) as s100 -->
  <KeyWord name="Fn'Print'Http'Error'Msg$" func="yes">
    <Overload retVal="s100">
      <Param name="status [in]" />
    </Overload>	
  </KeyWord>      
  
  <!-- Fn'ProgVer$(progspec$ as s260) as s14 -->
  <KeyWord name="Fn'ProgVer$" func="yes">
    <Overload retVal="[sosfunc:fnprogver.bsi] s14">
      <Param name="progspec$ as s260:in" />
    </Overload>	
  </KeyWord>      
  
  <!-- Fn'RGB(r as b1:inputonly, g as b1:inputonly, b as b1:inputonly) as b4 -->
  <KeyWord name="Fn'RGB" func="yes">
    <Overload retVal="[sosfunc:fnrgb.bsi] b4">
      <Param name="r as b1:in" />
      <Param name="g as b1:in" />
      <Param name="b as b1:in" />
    </Overload>	
  </KeyWord>      
  
  <!-- Fn'SplitPath$(path$ as s0:inputonly, fname$ as s0:outputonly) as s0 -->
  <KeyWord name="Fn'SplitPath$" func="yes">
    <Overload retVal="[sosfunc:fnsplitpth.bsi] s0">
      <Param name="path$ as s0:in" />
      <Param name="fname$ as s0:out" />
    </Overload>	
  </KeyWord>      
  
  <!-- Fn'Sprintf$() as S1024 -->
  <KeyWord name="Fn'Sprintf$" func="yes">
    <Overload retVal="[sosfunc:fnsprintf.bsi] s1024">
      <Param name="fmt$ as s1024:in" />
      <Param name="{arg1 as s100:in" />
      <Param name="..." />
      <Param name="argN as s100:in}" />    
    </Overload>	
  </KeyWord>      
  
  <!-- fn'str_pad$(src$ as s0:inputonly, padchr as s1:inputonly, &
                    flag as b1:inputonly, padlen as b2:inputonly) as s0 -->
  <KeyWord name="fn'str'pad$" func="yes">
    <Overload retVal="[sosfunc:fnstrpad.bsi] s0">
      <Param name="src$ as s0:in" />
      <Param name="padchr$ as s1:in" />
      <Param name="flag as b1:in [0=pad right 1=left 2=both]" />
      <Param name="padlen as b2:in" />
    </Overload>	
  </KeyWord>      
                    
  <!-- Fn'StrToFile(xbuf as x0:inputonly, fspec$ as s260:inputonly, overwrite as b1) as i4 -->
  <KeyWord name="Fn'StrToFile" func="yes">
    <Overload retVal="[sosfunc:fnfilestr.bsi] num">
      <Param name="xbuf as x0:in" />
      <Param name="fspec$ as s260:in" />
      <Param name="overwrite as b1:in" />
    </Overload>	
  </KeyWord>      
  
  <!-- fn'str'replace$(hsearch'needle$,hreplace'needle$,string$ as x STRPL_STRSIZ) as x STRPL_STRSIZ -->
  <KeyWord name="fn'str'replace$" func="yes">
    <Overload retVal="[sosfunc:strrpl.bsi] s4096">
      <Param name="hsearch'needle$ as s10:in" />
      <Param name="hreplace'needle$ as s10:in" />
      <Param name="string$ as s4096:in" />
    </Overload>	
  </KeyWord>      
  
  <!-- Fn'Sync'SBX'to'ATE(sbxname$) -->
  <KeyWord name="Fn'Sync'SBX'to'ATE" func="yes">
    <Overload retVal="[sosfunc:fnsyncsbx.bsi] num">
      <Param name="sbxname$ as s10:in" />
    </Overload>	
  </KeyWord>      
 
  <!-- Fn'TCPrecv$(socket as f6:inputonly, reqcnt as i2:inputonly, &
                    timer as b4:inputonly, status as f6:outputonly) as S1024 -->
  <KeyWord name="Fn'TCPrecv$" func="yes">
    <Overload retVal="s1024">
      <Param name="socket [in]" />
      <Param name="reqcnt [in]" />
      <Param name="timer [in]" />
      <Param name="status [out]" />
    </Overload>	
  </KeyWord>      

  <!-- Fn'To'Amos(nativespec$ as s0:INPUTONLY, amosspec$ as s0:OUTPUTONLY) as i2 -->
  <KeyWord name="Fn'To'Amos" func="yes">
    <Overload retVal="[sosfunc:fntoamos.bsi] bool">
      <Param name="nativespec$ as s0:in" />
      <Param name="amosspec$ as s0:out" />
    </Overload>	
  </KeyWord>      
  
  <!-- Fn'URL'Encode$(ansi$ as s0) as s0 -->
  <KeyWord name="Fn'URL'Encode$" func="yes">
    <Overload retVal="[sosfunc:fnurlenc.bsi] s0">
      <Param name="ansistring$ as s0:in" />
    </Overload>	
  </KeyWord>      
  
  <!-- Fn'UTF8toGDI$(utf8$ as s0) as s0 -->
  <KeyWord name="Fn'UTF8toGDI$" func="yes">
    <Overload retVal="[sosfunc:utf8togdi.bsi] s0">
      <Param name="utf8$ as s0:in" />
    </Overload>	
  </KeyWord>      
  
</AutoComplete>